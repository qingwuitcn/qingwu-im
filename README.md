## Qingwu IM 青梧 IM

![logo](https://gitee.com/qingwuitcn/qingwu-im/raw/main/chat_vue3/src/assets/logo.png "logo")

> 这是一个 IM 系统，内置好友，群聊，AI 功能（ai 需要自己搭建 ollama 和修改代码）

## Need environment 需要环境和已有功能

- golang2.1 + Vue3 前后分离
- 支持二次开发
- mysql >= 5.7 未测试 其他版本

## Demo address 演示地址

[[Demo Link]](http://im.qingwuit.com "Click It")

![pic](https://gitee.com/qingwuitcn/qingwu-im/raw/main/chat_vue3/src/assets/pic1.jpg "pic")

![pic](https://gitee.com/qingwuitcn/qingwu-im/raw/main/chat_vue3/src/assets/pic2.jpg "pic")

## Use 直接使用

```bash

# 去项目地址右侧【发行版】下载最新版


# 配置数据库  .env.bak 复制为 .env 进去填写
vim ./.env

# 加载数据结构
./qwim migrate up

# 导入 database里面的用户数据 ./databases.sql

# 运行主程序
./qwim serve

```

## Documentation 调试二开

下载代码后**chat_go** 目录为 golang 代码

```bash

# 进入chat_go
cd chat_go
go mod tidy

# 配置数据库  .env.bak 复制为 .env 进去填写
cp ./.env.bak ./.env

# 加载数据结构
go run main.go migrate up

# 运行主程序
go run main.go

```

下载代码后**chat_vue3** 目录为前端代码

```bash

# 由根目录进入chat_vue3
cd chat_vue3

# 下载依赖包
npm install # yarn install

# 运行前端
npm run dev  #  npm run build

```

## Nginx 伪静态配置

```bash
location / {
	try_files $uri $uri/ /index.html;
}
location /api {
    rewrite ^/api/(.*) /v1/$1 break; # 这里进行路径替换
	  proxy_pass http://127.0.0.1:3006; # 后端服务器地址和端口
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
}
location ^~ /uploads/ {
    alias /xxxxxx/public/uploads/;
    autoindex on;
    try_files $uri @backend; #
}
location @backend {
    proxy_set_header X-Forwarded-For $remote_addr;
    proxy_set_header Host            $http_host;
    proxy_pass http://127.0.0.1:3006;
}

location /api/ws {
    rewrite ^/api/(.*) /v1/$1 break; # 这里进行路径替换
    proxy_pass http://127.0.0.1:3006;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header X-Real-IP $remote_addr;
}

location /api/sse_ai {
    rewrite ^/api/(.*) /v1/$1 break; # 这里进行路径替换
    proxy_pass http://127.0.0.1:3006;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_cache_bypass $http_upgrade;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_buffering off; # 禁用缓冲
}

```
