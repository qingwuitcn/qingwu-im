go run main.go make ctr v1/auth | pro make ctr v1/auth
go run main.go make model auth | pro make model auth
go run main.go make migration auth | pro make migration auth
go run main.go migrate up
go run main.go migrate down

# Linux/AMD64

set CGO_ENABLED=0
set GOOS=linux
set GOARCH=amd64

# Windows/AMD64

set CGO_ENABLED=0
set GOOS=windows
set GOARCH=amd64

# Darwin/AMD64 Mac

set CGO_ENABLED=0
set GOOS=darwin
set GOARCH=amd64

go build -ldflags "-s -w" -o myprogram
