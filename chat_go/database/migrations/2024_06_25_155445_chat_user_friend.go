package migrations

import (
	"database/sql"
	"pro/app/models"
	"pro/pkg/migrate"

	"gorm.io/gorm"
)

// 好友关联表
func init() {

	type ChatUserFriend struct {
		models.BaseModel

		SendId    uint64 `gorm:"type:bigint unsigned;not null;default:0;comment:发送请求ID"`
		ReceiveId uint64 `gorm:"type:bigint unsigned;not null;default:0;comment:接收ID"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&ChatUserFriend{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&ChatUserFriend{})
	}

	migrate.Add("2024_06_25_155445_chat_user_friend", up, down)
}
