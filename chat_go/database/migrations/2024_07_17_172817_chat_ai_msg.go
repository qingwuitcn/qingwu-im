package migrations

import (
    "database/sql"
    "pro/app/models"
    "pro/pkg/migrate"

    "gorm.io/gorm"
)

func init() {

    type ChatAiMsg struct {
        models.BaseModel

		GroupId      uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:群组ID"`
		SendId       uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:发送请求ID"`
		ReceiveId    uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:接收ID"`
		Code         string `gorm:"type:varchar(30);not null;default:'';comment:请求码"`
		ShowDiffTime uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:是否显示距离当前时间"`
		IsType       uint8  `gorm:"type:tinyint unsigned;not null;default:1;index;comment:1文字-2图片-3文件-4分享-5语音-6系统消息-7通知消息"`
		Content      string `gorm:"type:text;comment:提示词"`
		AiContent      string `gorm:"type:text;comment:ai回答"`
		HiddenType   uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:1隐藏 该条记录"`
		PassType     uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:1同意 2拒绝"`

        models.CommonTimestampsField
    }

    up := func(migrator gorm.Migrator, DB *sql.DB) {
        migrator.AutoMigrate(&ChatAiMsg{})
    }

    down := func(migrator gorm.Migrator, DB *sql.DB) {
        migrator.DropTable(&ChatAiMsg{})
    }

    migrate.Add("2024_07_17_172817_chat_ai_msg", up, down)
}