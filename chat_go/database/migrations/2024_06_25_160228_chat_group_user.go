package migrations

import (
	"database/sql"
	"pro/app/models"
	"pro/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type ChatGroupUser struct {
		models.BaseModel

		BelongId      uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:用户ID"`
		GroupId       uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:群ID"`
		MapId         uint64 `gorm:"type:bigint unsigned;not null;default:0;comment:群位置索引ID"`
		CloseReminder uint8  `gorm:"type:tinyint unsigned;not null;default:1;comment:是否关闭提醒"`
		LastReadMsgId uint64 `gorm:"type:bigint unsigned;not null;default:0;comment:最后一次读取信息ID"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&ChatGroupUser{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&ChatGroupUser{})
	}

	migrate.Add("2024_06_25_160228_chat_group_user", up, down)
}
