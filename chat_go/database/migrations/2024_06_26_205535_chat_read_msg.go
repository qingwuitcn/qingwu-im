package migrations

import (
	"database/sql"
	"pro/app/models"
	"pro/pkg/migrate"

	"gorm.io/gorm"
)

// 信息阅读情况
func init() {

	type ChatReadMsg struct {
		models.BaseModel

		GroupId       uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:群组ID"`
		MsgId         uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:信息ID"`
		ReadStatus    []byte `gorm:"type:blob;comment:位图存储0/1"`
		IsRead        uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:普通读取状态"`
		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&ChatReadMsg{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&ChatReadMsg{})
	}

	migrate.Add("2024_06_26_205535_chat_read_msg", up, down)
}
