package migrations

import (
	"database/sql"
	"pro/app/models"
	"pro/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type ChatMsg struct {
		models.BaseModel

		GroupId      uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:群组ID"`
		SendId       uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:发送请求ID"`
		ReceiveId    uint64 `gorm:"type:bigint unsigned;not null;default:0;index;comment:接收ID"`
		Code         string `gorm:"type:varchar(30);not null;default:'';comment:系统消息类型"`
		ShowDiffTime uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:是否显示距离当前时间"`
		IsType       uint8  `gorm:"type:tinyint unsigned;not null;default:1;index;comment:1文字-2图片-3文件-4分享-5语音-6系统消息-7通知消息"`
		Content      string `gorm:"type:text;comment:具体聊天内容"`
		HiddenType   uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:1隐藏 该条记录"`
		PassType     uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:1同意 2拒绝"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&ChatMsg{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&ChatMsg{})
	}

	migrate.Add("2024_06_26_201933_chat_msg", up, down)
}

// SELECT DISTINCT
// CASE
// WHEN send_id = belong_id THEN receive_id
// ELSE send_id
// END AS chat_partner_id
// FROM
// chat_msg
// WHERE
// send_id = belong_id OR receive_id = YOUR_USER_ID;

// SELECT
//     DISTINCT
//     CASE
//         WHEN m.group_id IS NOT NULL AND m.group_id <> 0 THEN 'group'
//         ELSE 'individual'
//     END AS chat_type,
//     COALESCE(g.group_name, u.username) AS chat_partner_name,
//     CASE
//         WHEN m.group_id IS NOT NULL AND m.group_id <> 0 THEN m.group_id
//         ELSE COALESCE(m.send_id, m.receive_id)
//     END AS chat_partner_id
// FROM
//     your_message_table m
//     LEFT JOIN users u ON m.send_id = u.user_id OR m.receive_id = u.user_id
//     LEFT JOIN groups g ON m.group_id = g.group_id
// WHERE
//     m.send_id = YOUR_USER_ID OR m.receive_id = YOUR_USER_ID
// GROUP BY
//     chat_type, chat_partner_name, chat_partner_id;

// 每次查出聊天列表需要把未读取 消息标记 // 另外一个接口, 私聊可以根据receid 查询对应的read msg
// 群聊的话得 把所有自己群聊列出来 mapid 查出所有聊天ID 联查统计blob 自己索引是否为1 这里查出来golong 进行处理再返回
