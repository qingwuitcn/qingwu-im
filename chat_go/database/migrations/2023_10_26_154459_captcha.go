package migrations

import (
	"database/sql"
	"pro/app/models"
	"pro/pkg/migrate"

	"gorm.io/gorm"
)

// 前端请求后端(创建[Username+key+Captcha]) -> 得到图片正确位置(Key 密钥)=>MD5 -> 前端拖动最终位置

func init() {

	type Captcha struct {
		models.BaseModel

		Username   string `gorm:"type:varchar(30);not null;default:'';comment:账号"`
		PrivateKey string `gorm:"type:varchar(8);not null;default:'';comment:密钥"`
		Sign       string `gorm:"type:varchar(32);not null;default:'';comment:签名"`
		Captcha    string `gorm:"type:varchar(10);not null;default:'';comment:位置"`
		Ip         string `gorm:"type:varchar(20);not null;default:'0.0.0.0';comment:IP"`
		State      uint8  `gorm:"type:tinyint unsigned;not null;default:0;comment:状态"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Captcha{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Captcha{})
	}

	migrate.Add("2023_10_26_154459_captcha", up, down)
}
