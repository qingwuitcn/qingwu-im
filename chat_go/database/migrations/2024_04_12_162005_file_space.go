package migrations

import (
	"database/sql"
	"pro/app/models"
	"pro/pkg/migrate"

	"gorm.io/gorm"
)

// 图片空间
func init() {

	type FileSpace struct {
		models.BaseModel

		BelongId   uint64 `gorm:"type:int unsigned;not null;default:0;comment:所属"`
		DirId      uint64 `gorm:"type:int unsigned;not null;default:0;comment:所属文件夹"`
		Provider   string `gorm:"type:varchar(20);not null;default:'';comment:用户类型 Admin Partner User"`
		Name       string `gorm:"type:varchar(60);not null;default:'';comment:文件原名"`
		NewName    string `gorm:"type:varchar(60);not null;default:'';comment:新文件名"`
		Md5        string `gorm:"type:varchar(32);not null;default:'';comment:Md5值"`
		Url        string `gorm:"type:varchar(150);not null;default:'';comment:文件地址"`
		ExtName    string `gorm:"type:varchar(10);not null;default:'';comment:文件后缀"`
		DiskName   string `gorm:"type:varchar(10);not null;default:'';comment:储存位置 public-本地aliyun-qiniu"`
		Info       string `gorm:"type:varchar(30);not null;default:'';comment:文件信息"`
		Status     uint8  `gorm:"type:tinyint;not null;default:0;comment:状态"`
		UploadType uint8  `gorm:"type:tinyint;not null;default:1;comment:上传类型1-单文件2-分片"`
		IsDir      uint8  `gorm:"type:tinyint;not null;default:1;comment:是否文件夹1-文件夹2-文件"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&FileSpace{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&FileSpace{})
	}

	migrate.Add("2024_04_12_162005_file_space", up, down)
}
