package migrations

import (
	"database/sql"
	"pro/app/models"
	"pro/pkg/migrate"

	"gorm.io/gorm"
)

// 群聊
func init() {

	type ChatGroup struct {
		models.BaseModel

		BelongId uint64 `gorm:"type:bigint unsigned;not null;default:0;comment:所属用户"`
		Name     string `gorm:"type:varchar(30);not null;default:'';comment:名称"`
		Avatar   string `gorm:"type:varchar(150);not null;default:'';comment:头像"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&ChatGroup{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&ChatGroup{})
	}

	migrate.Add("2024_06_25_160058_chat_group", up, down)
}
