package migrations

import (
    "database/sql"
    "pro/app/models"
    "pro/pkg/migrate"

    "gorm.io/gorm"
)

func init() {

    type ChatUser struct {
        models.BaseModel

        BelongId    uint64  `gorm:"type:bigint unsigned;not null;default:0;comment:所属"`
		Username    string  `gorm:"type:varchar(30);not null;index;unique;comment:账号"`
		Nickname    string  `gorm:"type:varchar(30);not null;default:'';comment:昵称"`
		Avatar      string  `gorm:"type:varchar(150);not null;default:'';comment:头像"`
		Phone       string  `gorm:"type:varchar(20);not null;default:'';comment:手机"`
		Email       string  `gorm:"type:varchar(40);not null;default:'';comment:邮箱"`
		Password    string  `gorm:"type:varchar(80);not null;default:'';comment:密码"`
		PayPassword string  `gorm:"type:varchar(80);not null;default:'';comment:支付密码"`
		Money       float64 `gorm:"type:decimal(9,2) unsigned;not null;default:0.00;comment:余额"`

        models.CommonTimestampsField
    }

    up := func(migrator gorm.Migrator, DB *sql.DB) {
        migrator.AutoMigrate(&ChatUser{})
    }

    down := func(migrator gorm.Migrator, DB *sql.DB) {
        migrator.DropTable(&ChatUser{})
    }

    migrate.Add("2024_06_25_154156_chat_user", up, down)
}