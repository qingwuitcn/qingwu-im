package file_space

import (
	"pro/app/requests"
	"pro/pkg/database"
	"pro/pkg/helpers"
	"pro/pkg/paginator"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Get(idstr string) (fileSpace FileSpace) {
    database.DB.Where("id", idstr).First(&fileSpace)
    return
}

func GetIn(idstr string) (fileSpace []FileSpace) {
	database.DB.Find(&fileSpace, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (fileSpace FileSpace) {
    database.DB.Where(field + " = ?" , value).First(&fileSpace)
    return
}

func All() (fileSpaces []FileSpace) {
    database.DB.Find(&fileSpaces)
    return 
}

func Paginate(c *gin.Context, request *requests.FileSpaceRequest, perPage int) (fileSpaces []FileSpace, paging paginator.Paging) {
    model := SearchModel(request)
    paging = paginator.Paginate(
        c,
        model,
        &fileSpaces,
        perPage,
    )
    return
}

func SearchModel(request *requests.FileSpaceRequest) *gorm.DB{
	model := database.DB.Model(FileSpace{})
	if !helpers.Empty(request.Name) {
		  model = model.Where("name like ?","%" + request.Name + "%")
	}
	return model
}

func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(FileSpace{}).Where(field + " = ?",  value).Count(&count)
    return count > 0
}
