//Package file_space 模型
package file_space

import (
    "pro/app/models"
    "pro/pkg/database"
    "strings"
)

type FileSpace struct {
    models.BaseModel

    BelongId   uint64 `json:"belong_id"`
	DirId      uint64 `json:"dir_id"`
	Provider   string `json:"provider"`
	Name       string `json:"name"`
	NewName    string `json:"new_name"`
	Md5        string `json:"md5"`
	Url        string `json:"url"`
	ExtName    string `json:"ext_name"`
	DiskName   string `json:"disk_name"`
	Info       string `json:"info"`
	Status     uint8  `json:"status"`
	UploadType uint8  `json:"upload_type"`
	IsDir      uint8  `json:"is_dir"`

    models.CommonTimestampsField
}

type FileResource struct {
	Name       string `json:"name"`
	NewName    string `json:"new_name"`
	Md5        string `json:"md5"`
	Url        string `json:"url"`
	ExtName    string `json:"ext_name"`
	DiskName   string `json:"disk_name"`
}

func (fileSpace *FileSpace) Create() {
    database.DB.Create(&fileSpace)
}

func (fileSpace *FileSpace) Save() (rowsAffected int64) {
    result := database.DB.Save(&fileSpace)
    return result.RowsAffected
}

func (fileSpace *FileSpace) Delete() (rowsAffected int64) {
    result := database.DB.Delete(&fileSpace)
    return result.RowsAffected
}

func (fileSpace *FileSpace) DeleteAll(idstr string) (rowsAffected int64) {
    result := database.DB.Delete(&FileSpace{}, strings.Split(idstr, ","))
	return result.RowsAffected
}