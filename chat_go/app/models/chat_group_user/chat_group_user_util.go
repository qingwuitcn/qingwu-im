package chat_group_user

import (
	"pro/pkg/database"
	"strings"
)

func Get(idstr string) (chatGroupUser ChatGroupUser) {
    database.DB.Where("id", idstr).First(&chatGroupUser)
    return
}

func GetIn(idstr string) (chatGroupUser []ChatGroupUser) {
	database.DB.Find(&chatGroupUser, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (chatGroupUser ChatGroupUser) {
    database.DB.Where(field + " = ?" , value).First(&chatGroupUser)
    return
}

func GetByUserAndGroup(uid, gid string) (chatGroupUser ChatGroupUser) {
    database.DB.Where("belong_id = ? and group_id = ?" , uid,gid).First(&chatGroupUser)
    return
}

func GetByUserAndGroupExist(uid, gid string) bool {
	var count int64
    database.DB.Model(ChatGroupUser{}).Where("belong_id = ? and group_id = ?" , uid,gid).Count(&count)
    return count > 0
}
func GetMaxMapId(gid string) uint64{
	type Maps struct {
		MapId uint64
	}
	var maps Maps
    database.DB.Model(ChatGroupUser{}).Select("max(map_id) as map_id").Where("group_id = ?" , gid).First(&maps)
    return maps.MapId
}

func All() (chatGroupUsers []ChatGroupUser) {
    database.DB.Find(&chatGroupUsers)
    return 
}

func AllBy(field, value string) (chatGroupUsers []ChatGroupUser) {
    database.DB.Where(field + " = ?" , value).Order("map_id asc").Find(&chatGroupUsers)
    return 
}

// 如果是跨表，还需要跨表联查
func AllByJoinUser(field, value string) (chatGroupUsers []ChatGroupUserExt) {
    database.DB.Where(field + " = ?" , value).
	Model(ChatGroupUser{}).
	Select("chat_group_users.*,u.nickname,u.avatar").
	Joins("inner join chat_users as u on u.id=chat_group_users.belong_id").
	Order("map_id asc").Find(&chatGroupUsers)
    return 
}

func BatchCreate(chatGroupUsers []ChatGroupUser){
	database.DB.Save(&chatGroupUsers)
}

// 判断是否是群成员
func IsGroupMember(uid,groupId string) bool{
	var count int64
	database.DB.Model(ChatGroupUser{}).Where("belong_id = ? and group_id = ?",  uid,groupId).Count(&count)
	return count > 0
}

// 获取我所属的群ID
func GetMyGroupId(uid string) (groupId []uint64){
	database.DB.Model(ChatGroupUser{}).Select("group_id").Where("belong_id = ?",uid).Find(&groupId)
	return
}

// 获取群成员ID
func GetGroupUserId(gid string) (belongId []uint64){
	database.DB.Model(ChatGroupUser{}).Select("belong_id").Where("group_id = ?",gid).Find(&belongId)
	return
}
// func Paginate(c *gin.Context, request *requests.ChatGroupUserRequest, perPage int) (chatGroupUsers []ChatGroupUser, paging paginator.Paging) {
//     model := SearchModel(request)
//     paging = paginator.Paginate(
//         c,
//         model,
//         &chatGroupUsers,
//         perPage,
//     )
//     return
// }

// func SearchModel(request *requests.ChatGroupUserRequest) *gorm.DB{
// 	model := database.DB.Model(ChatGroupUser{})
// 	// if !helpers.Empty(request.Name) {
// 	//	  model = model.Where("name like ?","%" + request.Name + "%")
// 	// }
// 	return model
// }

func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(ChatGroupUser{}).Where(field + " = ?",  value).Count(&count)
    return count > 0
}

func DeleteAllByGroup(idstr string) (rowsAffected int64) {
	result := database.DB.Where("group_id = ?",idstr).Delete(&ChatGroupUser{})
	return result.RowsAffected
}


