// Package chat_group_user 模型
package chat_group_user

import (
	"pro/app/models"
	"pro/pkg/database"
	"strings"
)

type ChatGroupUser struct {
	models.BaseModel

	BelongId      uint64 `json:"belong_id"`
	GroupId       uint64 `json:"group_id"`
	MapId         uint64 `json:"map_id"`
	CloseReminder uint8  `json:"close_reminder"`
	LastReadMsgId uint64 `json:"last_read_msg_id"`
	Nickname     string `json:"nickname" gorm:"-"`
	Avatar     string `json:"avatar" gorm:"-"`

	models.CommonTimestampsField
}

type ChatGroupUserExt struct {
	models.BaseModel

	BelongId      uint64 `json:"belong_id"`
	GroupId       uint64 `json:"group_id"`
	MapId         uint64 `json:"map_id"`
	CloseReminder uint8  `json:"close_reminder"`
	LastReadMsgId uint64 `json:"last_read_msg_id"`
	Nickname     string `json:"nickname"`
	Avatar     string `json:"avatar"`

	models.CommonTimestampsField
}

func (chatGroupUser *ChatGroupUser) Create() {
	database.DB.Create(&chatGroupUser)
}

func (chatGroupUser *ChatGroupUser) Save() (rowsAffected int64) {
	result := database.DB.Save(&chatGroupUser)
	return result.RowsAffected
}

func (chatGroupUser *ChatGroupUser) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&chatGroupUser)
	return result.RowsAffected
}

func (chatGroupUser *ChatGroupUser) DeleteAll(idstr string) (rowsAffected int64) {
	result := database.DB.Delete(&ChatGroupUser{}, strings.Split(idstr, ","))
	return result.RowsAffected
}
