package chat_user

import (
	"pro/app/requests"
	"pro/pkg/database"
	"pro/pkg/helpers"
	"pro/pkg/paginator"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Get(idstr string) (chatUser ChatUser) {
	database.DB.Where("id", idstr).First(&chatUser)
	return
}

// GetByMulti 通过 手机号/Email/用户名 来获取用户
func GetByMulti(loginID string) (chatUser ChatUser) {
	database.DB.
		Or("id = ?", loginID).
		Or("phone = ?", loginID).
		Or("email = ?", loginID).
		Or("username = ?", loginID).
		First(&chatUser)
	return
}

func GetIn(idstr string) (chatUser []ChatUser) {
	database.DB.Find(&chatUser, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (chatUser ChatUser) {
	database.DB.Where(field+" = ?", value).First(&chatUser)
	return
}

func All() (chatUser []ChatUser) {
	database.DB.Find(&chatUser) // 排序
	return
}

func AllBy(field, value string) (chatUser []ChatUser) {
	database.DB.Where(field+" = ?", value).Find(&chatUser) // 排序
	return
}


// Paginate 分页内容
func Paginate(c *gin.Context, request *requests.ChatUserRequest, perPage int) (chatUser []ChatUser, paging paginator.Paging) {
    model := SearchModel(request)
    paging = paginator.Paginate(
        c,
        model,
        &chatUser,
        perPage,
    )
    return
}

func SearchModel(request *requests.ChatUserRequest) *gorm.DB{
	model := database.DB.Model(ChatUser{})
	if !helpers.Empty(request.Username) {
		  model = model.Where("username like ?","%" + request.Username + "%")
	}
	if !helpers.Empty(request.Nickname) {
		model = model.Where("nickname like ?","%" + request.Nickname + "%")
  	}
	if !helpers.Empty(request.Phone) {
		model = model.Where("phone like ?","%" + request.Phone + "%")
  	}
	if !helpers.Empty(request.Email) {
		model = model.Where("email like ?","%" + request.Email + "%")
  	}
	return model
}



