// Package admin_permission 模型
package chat_user

import (
	"pro/app/models"
	"pro/pkg/database"
	"pro/pkg/hash"
	"strings"
)

type ChatUser struct {
	models.BaseModel

	BelongId    uint64  `json:"belong_id"`
	Username    string  `json:"username"`
	Nickname    string  `json:"nickname"`
	Avatar      string  `json:"avatar"`
	Phone       string  `json:"phone"`
	Email       string  `json:"email"`
	Password    string  `json:"-"`
	PayPassword string  `json:"-"`
	Money       float64 `json:"money"`

	models.CommonTimestampsField
}

type ChatUserFriend struct {
    models.BaseModel
    SendId uint64 `json:"send_id"`
    ReceiveId uint64 `json:"receive_id"`
}

func (chatUser *ChatUser) Create() {
	database.DB.Create(&chatUser)
}

func (chatUser *ChatUser) Save() (rowsAffected int64) {
	result := database.DB.Save(&chatUser)
	return result.RowsAffected
}

func (chatUser *ChatUser) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&chatUser)
	return result.RowsAffected
}

func (chatUser *ChatUser) DeleteAll(idstr string) (rowsAffected int64) {
	result := database.DB.Delete(&ChatUser{}, strings.Split(idstr, ","))
	return result.RowsAffected
}

// ComparePassword 密码是否正确
func (chatUser *ChatUser) ComparePassword(_password string) bool {
	return hash.BcryptCheck(_password, chatUser.Password)
}

