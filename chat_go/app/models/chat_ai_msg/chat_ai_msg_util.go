package chat_ai_msg

import (
	"pro/pkg/database"
	"strings"

)

func Get(idstr string) (chatAiMsg ChatAiMsg) {
    database.DB.Where("id", idstr).First(&chatAiMsg)
    return
}

func GetIn(idstr string) (chatAiMsg []ChatAiMsg) {
	database.DB.Find(&chatAiMsg, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (chatAiMsg ChatAiMsg) {
    database.DB.Where(field + " = ?" , value).First(&chatAiMsg)
    return
}

func All() (chatAiMsgs []ChatAiMsg) {
    database.DB.Find(&chatAiMsgs)
    return 
}

// 获取上下文多少条
func AllContextByLimit(sendId,receiveId string,limt int) (chatAiMsgs []ChatAiMsg) {
    database.DB.Where("send_id = ?",sendId).Where("receive_id = ?",receiveId).Order("id desc").Limit(limt).Find(&chatAiMsgs)
    return 
}

// func IsExist(field, value string) bool {
//     var count int64
//     database.DB.Model(ChatAiMsg{}).Where(field + " = ?",  value).Count(&count)
//     return count > 0
// }
