// Package chat_ai_msg 模型
package chat_ai_msg

import (
	"pro/app/models"
	"pro/pkg/database"
	"strings"
)

type ChatAiMsg struct {
	models.BaseModel

	GroupId      uint64 `json:"group_id"`
	SendId       uint64 `json:"send_id"`
	ReceiveId    uint64 `json:"receive_id"`
	Code         string `json:"code"`
	ShowDiffTime uint8  `json:"show_diff_time"`
	IsType       uint8  `json:"is_type"`
	Content      string `json:"content"`
	AiContent    string `json:"ai_content"`
	HiddenType   uint8  `json:"hidden_type"`
	PassType     uint8  `json:"pass_type"`

	models.CommonTimestampsField
}

func (chatAiMsg *ChatAiMsg) Create() {
	database.DB.Create(&chatAiMsg)
}

func (chatAiMsg *ChatAiMsg) Save() (rowsAffected int64) {
	result := database.DB.Save(&chatAiMsg)
	return result.RowsAffected
}

func (chatAiMsg *ChatAiMsg) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&chatAiMsg)
	return result.RowsAffected
}

func (chatAiMsg *ChatAiMsg) DeleteAll(idstr string) (rowsAffected int64) {
	result := database.DB.Delete(&ChatAiMsg{}, strings.Split(idstr, ","))
	return result.RowsAffected
}
