package chat_msg

import (
	"pro/app/models/chat_group_user"
	"pro/app/models/chat_user_friend"
	"pro/app/requests"
	"pro/pkg/database"
	"pro/pkg/helpers"
	"pro/pkg/paginator"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Get(idstr string) (chatMsg ChatMsg) {
    database.DB.Where("id", idstr).First(&chatMsg)
    return
}

func GetIn(idstr string) (chatMsg []ChatMsg) {
	database.DB.Find(&chatMsg, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (chatMsg ChatMsg) {
    database.DB.Where(field + " = ?" , value).Order("id desc").First(&chatMsg)
    return
}

func All() (chatMsgs []ChatMsg) {
    database.DB.Find(&chatMsgs)
    return 
}

func Paginate(c *gin.Context, request *requests.ChatMsgRequest, perPage int) (chatMsgs []ChatMsg, paging paginator.Paging) {
    model := SearchModel(request)
    paging = paginator.Paginate(
        c,
        model,
        &chatMsgs,
        perPage,
    )
    return
}

func SearchModel(request *requests.ChatMsgRequest) *gorm.DB{
	model := database.DB.Model(ChatMsg{})

	model = model.Where("is_type != 6")
	if !helpers.Empty(request.IsType) {
		model = model.Where("is_type = ?",request.IsType)
  	}else{
		model = model.Where("is_type > ?",0)
	}
	if !helpers.Empty(request.PageLastId) {
		  model = model.Where("id < ?",request.PageLastId)
	}
	if !helpers.Empty(request.SendId) && !helpers.Empty(request.ReceiveId) {
		model = model.Where("(send_id = ? and receive_id = ?) or (send_id = ? and receive_id = ?)",request.SendId,request.ReceiveId,request.ReceiveId,request.SendId)
	}

	if !helpers.Empty(request.GroupId) {
		model = model.Where("group_id = ?",request.GroupId)
	}
	if !helpers.Empty(request.Content) {
		model = model.Where("content like ?","%"+request.Content+"%")
	}
	return model
}

func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(ChatMsg{}).Where(field + " = ?",  value).Count(&count)
    return count > 0
}

// 未处理的系统信息
func IsExistUnprocessed(code,field, value,receive_id string) bool {
    var count int64
    database.DB.Model(ChatMsg{}).Where(field + " = ? and receive_id = ? and code = ? and pass_type = 0",  value,receive_id,code).Count(&count)
    return count > 0
}

// 获取正在聊天记录ID  这里需要根据 env DB_A_same 判断是通过 left join 还是 快库来处理 临时使用
func GetChatingList(selfId,nickname string) (chatPartner []ChatPartner){
	selectSql := " (CASE WHEN chat_msgs.send_id = ? THEN chat_msgs.receive_id ELSE chat_msgs.send_id END) AS chat_partner_id,"
	selectSql += "chat_msgs.is_type,"
	selectSql += "chat_msgs.content,"
	selectSql += "chat_msgs.code,"
	selectSql += "chat_msgs.id as msg_id,"
	selectSql += "chat_msgs.group_id,"
	selectSql += "chat_msgs.send_id,"
	selectSql += "chat_msgs.receive_id,"
	selectSql += "a.nickname as send_nickname,"
	selectSql += "a.avatar as send_avatar,"
	selectSql += "b.nickname as receive_nickname,"
	selectSql += "b.avatar as receive_avatar,"
	selectSql += "'' as group_nickname,"
	selectSql += "'' as group_avatar,"
	selectSql += "chat_msgs.created_at,"
	selectSql += "chat_msgs.updated_at"
	friendModel := database.DB.Model(ChatMsg{}).
	Select(selectSql,selfId).
	Joins("inner join (SELECT (CASE WHEN m.send_id = ? THEN m.receive_id ELSE m.send_id END ) AS chat_partner_id, max(id) as max_id from chat_msgs as m  where ( m.send_id = ? OR m.receive_id = ? ) AND m.is_type != 6 AND m.group_id = 0 GROUP BY chat_partner_id ) as temp on temp.max_id = chat_msgs.id AND chat_msgs.deleted_at IS NULL",selfId,selfId,selfId).
	Joins("left join chat_users as a on a.id = chat_msgs.send_id").
	Joins("left join chat_users as b on b.id = chat_msgs.receive_id").
	// Joins("left join chat_groups as c on c.id = chat_msgs.group_id").
	Joins("left join chat_user_friends as d on (d.send_id = chat_msgs.send_id and d.receive_id=chat_msgs.receive_id) or (d.send_id = chat_msgs.receive_id and d.receive_id=chat_msgs.send_id) and d.deleted_at is null").
	Where("d.id > 0").
	Where("a.nickname like ? or b.nickname like ?","%"+nickname+"%","%"+nickname+"%")

	// // 获取我所在的群组ID
	groupId:="-1"
	groupIds := chat_group_user.GetMyGroupId(selfId)
	if len(groupIds) > 0 {
		groupId = helpers.SliceToString(groupIds)
	}
	selectGroupSql := "chat_msgs.group_id AS chat_partner_id,"
	selectGroupSql += "chat_msgs.is_type,"
	selectGroupSql += "chat_msgs.content,"
	selectGroupSql += "chat_msgs.code,"
	selectGroupSql += "chat_msgs.id as msg_id,"
	selectGroupSql += "chat_msgs.group_id,"
	selectGroupSql += "chat_msgs.send_id,"
	selectGroupSql += "chat_msgs.receive_id,"
	selectGroupSql += "'' as send_nickname,"
	selectGroupSql += "'' as send_avatar,"
	selectGroupSql += "'' as receive_nickname,"
	selectGroupSql += "'' as receive_avatar,"
	selectGroupSql += "c.name as group_nickname,"
	selectGroupSql += "c.avatar as group_avatar,"
	selectGroupSql += "chat_msgs.created_at,"
	selectGroupSql += "chat_msgs.updated_at"
	groupModel := database.DB.Model(ChatMsg{}).
	Select(selectGroupSql).
	Joins("inner join ( select group_id,max(id) as max_id from chat_msgs as m  where m.is_type != 6 and m.group_id in ("+groupId+") AND deleted_at IS NULL GROUP BY m.group_id ) as temp on temp.group_id=chat_msgs.group_id and temp.max_id = chat_msgs.id AND deleted_at IS NULL").
	// Joins("left join chat_users as a on a.id = chat_msgs.send_id").
	// Joins("left join chat_users as b on b.id = chat_msgs.receive_id").
	Joins("left join chat_groups as c on c.id = chat_msgs.group_id and c.deleted_at is null").
	Where("chat_msgs.is_type != 6 and chat_msgs.group_id in ("+groupId+")").
	Where("c.name like ? ","%"+nickname+"%")

	// 获取系统消息
	selectSystemSql := " 0 AS chat_partner_id,"
	selectSystemSql += "chat_msgs.is_type,"
	selectSystemSql += "chat_msgs.content,"
	selectSystemSql += "chat_msgs.code,"
	selectSystemSql += "chat_msgs.id as msg_id,"
	selectSystemSql += "chat_msgs.group_id,"
	selectSystemSql += "chat_msgs.send_id,"
	selectSystemSql += "chat_msgs.receive_id,"
	selectSystemSql += "a.nickname as send_nickname,"
	selectSystemSql += "a.avatar as send_avatar,"
	selectSystemSql += "b.nickname as receive_nickname,"
	selectSystemSql += "b.avatar as receive_avatar,"
	selectSystemSql += "c.name as group_nickname,"
	selectSystemSql += "c.avatar as group_avatar,"
	selectSystemSql += "chat_msgs.created_at,"
	selectSystemSql += "chat_msgs.updated_at"
	systemModel := database.DB.Model(ChatMsg{}).
	Select(selectSystemSql).
	Joins("inner join (SELECT max(id) as max_id from chat_msgs as m where m.receive_id = ? and m.is_type = 6  AND m.code != '' ) as temp on temp.max_id = chat_msgs.id AND chat_msgs.deleted_at IS NULL",selfId).
	Where("chat_msgs.receive_id = ? and chat_msgs.is_type = 6",selfId).
	Joins("left join chat_users as a on a.id = chat_msgs.send_id").
	Joins("left join chat_users as b on b.id = chat_msgs.receive_id").
	Joins("left join chat_groups as c on c.id = chat_msgs.group_id")

	// Order("chat_msgs.created_at desc").
	// Find(&chatPartner)

	database.DB.Raw("? UNION ALL ? UNION ALL ? order by created_at desc",
		friendModel,
		groupModel,
		systemModel,
	).Scan(&chatPartner)

	return
}

// 获取正在聊天记录ID  这里需要根据 env DB_A_same 判断是通过 left join 还是 快库来处理 临时使用
func GetChatingSigle(uid,id,rid,is_type,group_id string) (chatPartner ChatPartner){
	if group_id == "0" && is_type !="6" {
		selectSql := ""
		if uid == id {
			selectSql += "receive_id AS chat_partner_id,"
		}else{
			selectSql += "send_id AS chat_partner_id,"
		}
		// selectSql := "(CASE WHEN chat_msgs.send_id = ? THEN chat_msgs.receive_id ELSE chat_msgs.send_id END) AS chat_partner_id,"
		selectSql += "chat_msgs.is_type,"
		selectSql += "chat_msgs.show_diff_time,"
		selectSql += "chat_msgs.content,"
		selectSql += "chat_msgs.code,"
		selectSql += "chat_msgs.id as msg_id,"
		selectSql += "chat_msgs.group_id,"
		selectSql += "chat_msgs.send_id,"
		selectSql += "chat_msgs.receive_id,"
		selectSql += "a.nickname as send_nickname,"
		selectSql += "a.avatar as send_avatar,"
		selectSql += "b.nickname as receive_nickname,"
		selectSql += "b.avatar as receive_avatar,"
		selectSql += "c.name as group_nickname,"
		selectSql += "c.avatar as group_avatar,"
		selectSql += "chat_msgs.created_at,"
		selectSql += "chat_msgs.updated_at"
		database.DB.Model(ChatMsg{}).
		Select(selectSql,id).
		Where("(chat_msgs.send_id = ? and chat_msgs.receive_id = ?) or (chat_msgs.send_id = ? and chat_msgs.receive_id = ?) and chat_msgs.is_type != 6 and chat_msgs.group_id = 0 ",id,rid,rid,id).
		Joins("left join chat_users as a on a.id = chat_msgs.send_id").
		Joins("left join chat_users as b on b.id = chat_msgs.receive_id").
		Joins("left join chat_groups as c on c.id = chat_msgs.group_id").
		Order("chat_msgs.created_at desc").
		First(&chatPartner)
	} 

	if group_id != "0" && is_type !="6" {
		// 获取我所在的群组ID
		selectGroupSql := "group_id AS chat_partner_id,"
		selectGroupSql += "chat_msgs.is_type,"
		selectGroupSql += "chat_msgs.content,"
		selectGroupSql += "chat_msgs.code,"
		selectGroupSql += "chat_msgs.id as msg_id,"
		selectGroupSql += "chat_msgs.group_id,"
		selectGroupSql += "chat_msgs.send_id,"
		selectGroupSql += "chat_msgs.receive_id,"
		selectGroupSql += "a.nickname as send_nickname,"
		selectGroupSql += "a.avatar as send_avatar,"
		selectGroupSql += "b.nickname as receive_nickname,"
		selectGroupSql += "b.avatar as receive_avatar,"
		selectGroupSql += "c.name as group_nickname,"
		selectGroupSql += "c.avatar as group_avatar,"
		selectGroupSql += "chat_msgs.created_at,"
		selectGroupSql += "chat_msgs.updated_at"
		database.DB.Model(ChatMsg{}).
		Select(selectGroupSql).
		Where("chat_msgs.is_type != 6 and chat_msgs.group_id in (?)",group_id).
		Joins("left join chat_users as a on a.id = chat_msgs.send_id").
		Joins("left join chat_users as b on b.id = chat_msgs.receive_id").
		Joins("left join chat_groups as c on c.id = chat_msgs.group_id").
		Order("chat_msgs.created_at desc").
		First(&chatPartner)
	}

	if is_type =="6" && id != "0" {
		// 获取系统消息
		selectSystemSql := "0 AS chat_partner_id,"
		selectSystemSql += "chat_msgs.is_type,"
		selectSystemSql += "chat_msgs.content,"
		selectSystemSql += "chat_msgs.code,"
		selectSystemSql += "chat_msgs.id as msg_id,"
		selectSystemSql += "chat_msgs.group_id,"
		selectSystemSql += "chat_msgs.send_id,"
		selectSystemSql += "chat_msgs.receive_id,"
		selectSystemSql += "a.nickname as send_nickname,"
		selectSystemSql += "a.avatar as send_avatar,"
		selectSystemSql += "b.nickname as receive_nickname,"
		selectSystemSql += "b.avatar as receive_avatar,"
		selectSystemSql += "c.name as group_nickname,"
		selectSystemSql += "c.avatar as group_avatar,"
		selectSystemSql += "chat_msgs.created_at,"
		selectSystemSql += "chat_msgs.updated_at"
		database.DB.Model(ChatMsg{}).
		Select(selectSystemSql).
		Where("chat_msgs.receive_id = ? and chat_msgs.is_type = 6",uid).
		Joins("left join chat_users as a on a.id = chat_msgs.send_id").
		Joins("left join chat_users as b on b.id = chat_msgs.receive_id").
		Joins("left join chat_groups as c on c.id = chat_msgs.group_id").
		Order("chat_msgs.created_at desc").
		First(&chatPartner)
	}

	return
}

// 获取某用户的未读信息
func GetFriendUnread(sendId,receiveId string) (count int64){
	database.DB.Model(ChatMsg{}).
	Joins("left join chat_read_msgs as r on r.msg_id = chat_msgs.id").
	Where("r.is_read = 0").
	Where("chat_msgs.send_id = ? and chat_msgs.receive_id = ? ",receiveId,sendId).
	Count(&count)
	return
}

// 获取某群的未读信息
func GetGroupUnread(uid,groupId string) (count int64){
	database.DB.Model(ChatMsg{}).Where("group_id = ?",groupId).
	Where("(select last_read_msg_id from chat_group_users where belong_id=? and group_id = ? and deleted_at is null limit 1)  < chat_msgs.id",uid,groupId).
	Count(&count)
	return
}

// 获取系统消息
func GetSystemLog(c *gin.Context,selfId string, perPage int) (chatPartner []ChatPartner, paging paginator.Paging){
	selectSql := "chat_msgs.is_type,"
	selectSql += "chat_msgs.id,"
	selectSql += "chat_msgs.content,"
	selectSql += "chat_msgs.code,"
	selectSql += "chat_msgs.group_id,"
	selectSql += "chat_msgs.send_id,"
	selectSql += "chat_msgs.receive_id,"
	selectSql += "chat_msgs.pass_type,"
	selectSql += "a.nickname as send_nickname,"
	selectSql += "a.avatar as send_avatar,"
	selectSql += "b.nickname as receive_nickname,"
	selectSql += "b.avatar as receive_avatar,"
	selectSql += "c.name as group_nickname,"
	selectSql += "c.avatar as group_avatar,"
	selectSql += "chat_msgs.created_at,"
	selectSql += "chat_msgs.updated_at"
	model := database.DB.Model(ChatMsg{}).
	Select(selectSql).
	Joins("left join chat_users as a on a.id = chat_msgs.send_id").
	Joins("left join chat_users as b on b.id = chat_msgs.receive_id").
	Joins("left join chat_groups as c on c.id = chat_msgs.group_id").
	Order("chat_msgs.created_at desc").
	Where("chat_msgs.receive_id = ? and is_type=6 and code != ''",selfId)
	paging = paginator.Paginate(
        c,
        model,
        &chatPartner,
        perPage,
    )
	return
}

// 获取查看上一次信息
func GetLastMsg(sendId,receiveId,groupId string)(chatMsg ChatMsg){
	database.DB.Model(ChatMsg{}).
	Where("(chat_msgs.send_id = ? and chat_msgs.receive_id = ?) or (chat_msgs.send_id = ? and chat_msgs.receive_id = ?) ",receiveId,sendId,sendId,receiveId).
	Where("group_id = ?",groupId).
	Order("id desc").
	First(&chatMsg)
	return
}


// 获取好友列表
func Groups(id,username string) (chatUser []ChatPartner){
	selectSql := "chat_group_users.group_id AS chat_partner_id,"
	selectSql += "chat_group_users.group_id,"
	selectSql += "chat_group_users.belong_id as send_id,"
	selectSql += "0 as receive_id,"
	selectSql += "a.nickname as send_nickname,"
	selectSql += "a.avatar as send_avatar,"
	selectSql += "'' as receive_nickname,"
	selectSql += "'' as receive_avatar,"
	selectSql += "c.name as group_nickname,"
	selectSql += "c.avatar as group_avatar,"
	selectSql += "chat_group_users.created_at,"
	selectSql += "chat_group_users.updated_at"
	model := database.DB.Model(chat_group_user.ChatGroupUser{}).
	Select(selectSql).
	Joins("left join chat_users as a on a.id = chat_group_users.belong_id").
	// Joins("left join chat_users as b on b.id = receive_id").
	Joins("left join chat_groups as c on c.id = chat_group_users.group_id and c.deleted_at is null").
	Where("chat_group_users.belong_id = ?", id)
	
	if !helpers.Empty(username) {
		model = model.Where("c.name like ?","%" + username + "%")
	}

	model.Find(&chatUser)

	return
}

// 获取好友列表
func Friends(id,username string) (chatUser []ChatPartner){
	selectSql := "CASE WHEN chat_user_friends.send_id = ? THEN chat_user_friends.receive_id ELSE chat_user_friends.send_id END AS chat_partner_id,"
	selectSql += "0 as group_id,"
	selectSql += "chat_user_friends.send_id,"
	selectSql += "chat_user_friends.receive_id,"
	selectSql += "a.nickname as send_nickname,"
	selectSql += "a.avatar as send_avatar,"
	selectSql += "b.nickname as receive_nickname,"
	selectSql += "b.avatar as receive_avatar,"
	selectSql += "chat_user_friends.created_at,"
	selectSql += "chat_user_friends.updated_at"
	model := database.DB.Model(chat_user_friend.ChatUserFriend{}).
	Select(selectSql,id).
	Joins("left join chat_users as a on a.id = send_id").
	Joins("left join chat_users as b on b.id = receive_id").
	Where("chat_user_friends.send_id = ? or chat_user_friends.receive_id = ?", id,id)
	
	if !helpers.Empty(username) {
		model = model.Where("(send_id = ? and receive_id= ?) or (send_id = ? and receive_id= ?) or a.username like ? or b.nickname like ?",id,username,username,id,"%" + username + "%","%" + username + "%")
	}

	model.Find(&chatUser)

	return
}

