// Package chat_msg 模型
package chat_msg

import (
	"pro/app/models"
	"pro/pkg/database"
	"strings"
)

type ChatMsg struct {
	models.BaseModel

	GroupId   uint64 `json:"group_id"`
	SendId    uint64 `json:"send_id"`
	ReceiveId uint64 `json:"receive_id"`

	Code         string   `json:"code"`
	ShowDiffTime uint8    `json:"show_diff_time"`
	IsType       uint8    `json:"is_type"`
	HiddenType   uint8    `json:"hidden_type"`
	PassType     uint8    `json:"pass_type"`
	Content      string   `json:"content"`
	ChatUser     ChatUser `json:"chat_user" gorm:"foreignKey:ID;references:SendId"`

	models.CommonTimestampsField
}

type ChatUser struct {
	models.BaseModel
	BelongId uint64 `json:"belong_id"`
	Nickname string `json:"nickname"`
	Avatar   string `json:"avatar"`
}

// 聊天伙伴信息，正在聊天的记录结构体
type ChatPartner struct {
	models.BaseModel
	MsgId           uint64 `json:"msg_id"`
	GroupId         uint64 `json:"group_id"`
	SendId          uint64 `json:"send_id"`
	ReceiveId       uint64 `json:"receive_id"`
	ChatPartnerId   uint64 `json:"chat_partner_id"`
	SendNickname    string `json:"send_nickname"`
	SendAvatar      string `json:"send_avatar"`
	ReceiveNickname string `json:"receive_nickname"`
	ReceiveAvatar   string `json:"receive_avatar"`
	GroupNickname   string `json:"group_nickname"`
	GroupAvatar     string `json:"group_avatar"`
	Content         string `json:"content"`
	Code            string `json:"code"`
	IsType          uint8  `json:"is_type"`
	ShowDiffTime    uint8  `json:"show_diff_time"`
	PassType        uint8  `json:"pass_type"`
	Unread          uint64 `json:"unread"` // 未读信息
	models.CommonTimestampsField
}

func (chatMsg *ChatMsg) Create() {
	database.DB.Create(&chatMsg)
}

func (chatMsg *ChatMsg) Save() (rowsAffected int64) {
	result := database.DB.Save(&chatMsg)
	return result.RowsAffected
}

func (chatMsg *ChatMsg) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&chatMsg)
	return result.RowsAffected
}

func (chatMsg *ChatMsg) DeleteAll(idstr string) (rowsAffected int64) {
	result := database.DB.Delete(&ChatMsg{}, strings.Split(idstr, ","))
	return result.RowsAffected
}
