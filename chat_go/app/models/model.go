package models

import (
	"fmt"
	"time"

	"database/sql/driver"
	"github.com/spf13/cast"
	"gorm.io/gorm"
)

// BaseModel 模型基类
type BaseModel struct {
	ID uint64 `gorm:"column:id;primaryKey;autoIncrement;" json:"id,omitempty"`
}

// CommonTimestampsField 时间戳
type CommonTimestampsField struct {
	CreatedAt *JsonTime      `gorm:"column:created_at;index;" json:"created_at,omitempty"`
	UpdatedAt *JsonTime      `gorm:"column:updated_at;index;" json:"updated_at,omitempty"`
	DeletedAt gorm.DeletedAt `gorm:"index;" json:"deleted_at"`
}

// FormatTimestampsField 格式化时间
type FormatTimestampsField struct {
	CreatedAt JsonTime `gorm:"column:created_at;index;" json:"created_at,omitempty"`
	UpdatedAt JsonTime `gorm:"column:updated_at;index;" json:"updated_at,omitempty"`
}

// 柱状图现状图
type Statistics struct {
	DateTime  string  `json:"date_time"`
	Num       float64    `json:"num"`
}

type JsonTime time.Time
type DateTime time.Time
type MonthTime time.Time

// 实现它的json序列化方法
func (T DateTime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(T).Format("2006-01-02"))
	return []byte(stamp), nil
}

// 实现它的json序列化方法
func (T MonthTime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(T).Format("2006-01"))
	return []byte(stamp), nil
}

// 实现它的json序列化方法
func (T JsonTime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(T).Format("2006-01-02 15:04:05"))
	return []byte(stamp), nil
}

// GetStringID 获取 ID 的字符串格式
func (a BaseModel) GetStringID() string {
	return cast.ToString(a.ID)
}

func (t *JsonTime) Scan(v interface{}) error {
	if value, ok := v.(time.Time); ok {
		*t = JsonTime(value)
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}

func (t JsonTime) Value() (driver.Value, error) {
	var zeroTime time.Time
	tlt := time.Time(t)
	if tlt.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return tlt, nil
}
