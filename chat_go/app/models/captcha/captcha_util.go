package captcha

import (
	"pro/pkg/database"
	"strings"
	"time"
)

func Get(idstr string) (captcha Captcha) {
	database.DB.Where("id", idstr).First(&captcha)
	return
}

func GetIn(idstr string) (captcha []Captcha) {
	database.DB.Find(&captcha, strings.Split(idstr, ","))
	return
}

// 根据签名，状态,时间获取五分钟前记录
func GetBySign(str string) (captcha Captcha) {
	s := time.Now().Add(-5*time.Minute) // 五分钟时间后
	database.DB.Where("sign = ? and state = 0 and created_at >= ?", str,s).First(&captcha)
	return
}

// 同一时间同一IP获取次数用的次数
// num 限制次数
func CountByIp(ip string, num int64) bool{
	var counts int64
	startTime := time.Now().Format(time.DateTime)
	endTime := time.Now().Format(time.DateTime)
	database.DB.Model(Captcha{}).Where("ip = ? ",ip).Where("created_at >= ? and  created_at <= ? ",startTime,endTime).Count(&counts)
	return counts >= num
}

// 验证滑动链接验证签名
func Check(sign string) bool {
	// adminModel.
	captchaModel := GetBySign(sign)

	if captchaModel.ID > 0 {
		captchaModel.State = 1 // 使用过的
		captchaModel.Save()
		return true
	}else{
		captchaModel.State = 2 // 失败的
		captchaModel.Save()
		return false
	}
}

