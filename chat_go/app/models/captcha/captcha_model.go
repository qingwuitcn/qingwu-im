// Package captcha 模型
package captcha

import (
	"pro/app/models"
	"pro/pkg/database"

	"strings"
)

type Captcha struct {
	models.BaseModel

	Username   string `json:"username"`
	PrivateKey string `json:"randstr"`
	Sign       string `json:"-"`
	Ip         string `json:"-"`
	Captcha    string `json:"captcha"`
	State      uint8  `json:"-"`

	models.CommonTimestampsField
}

func (captcha *Captcha) Create() {
	database.DB.Create(&captcha)
}

func (captcha *Captcha) Save() (rowsAffected int64) {
	result := database.DB.Save(&captcha)
	return result.RowsAffected
}

func (captcha *Captcha) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&captcha)
	return result.RowsAffected
}

func (captcha *Captcha) DeleteAll(idstr string) (rowsAffected int64) {
	result := database.DB.Delete(&Captcha{}, strings.Split(idstr, ","))
	return result.RowsAffected
}
