package chat_read_msg

import (
	"pro/pkg/database"
	"pro/pkg/helpers"
	"strings"
)

func Get(idstr string) (chatReadMsg ChatReadMsg) {
    database.DB.Where("id", idstr).First(&chatReadMsg)
    return
}

func GetIn(idstr string) (chatReadMsg []ChatReadMsg) {
	database.DB.Find(&chatReadMsg, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (chatReadMsg ChatReadMsg) {
    database.DB.Where(field + " = ?" , value).First(&chatReadMsg)
    return
}

func All() (chatReadMsgs []ChatReadMsg) {
    database.DB.Find(&chatReadMsgs)
    return 
}


func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(ChatReadMsg{}).Where(field + " = ?",  value).Count(&count)
    return count > 0
}

// 修改指定用户的所有未读信息为已读
func UpdateUnreadFriend(uid,rid string){
	var chatReadMsg []ChatReadMsg
	database.DB.Model(ChatReadMsg{}).Where("m.send_id = ? and m.receive_id = ?",uid,rid).
	Joins("inner join chat_msgs as m on m.id=chat_read_msgs.msg_id").
	Find(&chatReadMsg)
	var msgId []string
	if len(chatReadMsg) > 0 {
		for _,v := range chatReadMsg {
			msgId = append(msgId,helpers.Uint64ToString(v.ID) )
		}
	}
	database.DB.Model(ChatReadMsg{}).Where("id in("+strings.Join(msgId,",")+")").Update("is_read",1)
}

