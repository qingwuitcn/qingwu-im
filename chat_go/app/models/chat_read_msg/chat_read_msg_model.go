// Package chat_read_msg 模型
package chat_read_msg

import (
	"pro/app/models"
	"pro/pkg/database"
	"strings"
)

type ChatReadMsg struct {
	models.BaseModel

	GroupId    uint64 `json:"group_id"`
	MsgId      uint64 `json:"msg_id"`
	ReadStatus []byte `json:"read_status"`
	IsRead     uint8  `json:"is_read"`

	models.CommonTimestampsField
}

func (chatReadMsg *ChatReadMsg) Create() {
	database.DB.Create(&chatReadMsg)
}

func (chatReadMsg *ChatReadMsg) Save() (rowsAffected int64) {
	result := database.DB.Save(&chatReadMsg)
	return result.RowsAffected
}

func (chatReadMsg *ChatReadMsg) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&chatReadMsg)
	return result.RowsAffected
}

func (chatReadMsg *ChatReadMsg) DeleteAll(idstr string) (rowsAffected int64) {
	result := database.DB.Delete(&ChatReadMsg{}, strings.Split(idstr, ","))
	return result.RowsAffected
}
