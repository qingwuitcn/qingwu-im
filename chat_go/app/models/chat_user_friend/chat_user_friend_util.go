package chat_user_friend

import (
	"pro/pkg/database"
	"strings"

)

func Get(idstr string) (chatUserFriend ChatUserFriend) {
    database.DB.Where("id", idstr).First(&chatUserFriend)
    return
}

func GetIn(idstr string) (chatUserFriend []ChatUserFriend) {
	database.DB.Find(&chatUserFriend, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (chatUserFriend ChatUserFriend) {
    database.DB.Where(field + " = ?" , value).First(&chatUserFriend)
    return
}

func All() (chatUserFriends []ChatUserFriend) {
    database.DB.Find(&chatUserFriends)
    return 
}

// 查询两个ID是否存在对应好友
func IsFriend(selfId,friendId string) bool {
	var count int64
	database.DB.Model(ChatUserFriend{}).Where("(send_id = ? and receive_id = ?) or (send_id = ? and receive_id = ?)",  selfId,friendId,friendId,selfId).Count(&count)
	return count > 0
}
// 获取好友关联数据
func GetFriend(selfId,friendId string) (chatUserFriend ChatUserFriend) {
	database.DB.Model(ChatUserFriend{}).Where("(send_id = ? and receive_id = ?) or (send_id = ? and receive_id = ?)",  selfId,friendId,friendId,selfId).First(&chatUserFriend)
	return 
}

func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(ChatUserFriend{}).Where(field + " = ?",  value).Count(&count)
    return count > 0
}

// 获取我好友ID
func GetMyFriendId(uid string) (friendId []uint64){
	database.DB.Model(ChatUserFriend{}).Select("(CASE WHEN send_id = ? THEN receive_id ELSE send_id END) AS send_id",uid).Where("send_id = ? or receive_id = ?",uid,uid).Find(&friendId)
	return
}
