//Package chat_user_friend 模型
package chat_user_friend

import (
    "pro/app/models"
    "pro/pkg/database"
    "strings"
)

type ChatUserFriend struct {
    models.BaseModel

    SendId uint64 `json:"send_id"`
    ReceiveId uint64 `json:"receive_id"`

    models.CommonTimestampsField
}

func (chatUserFriend *ChatUserFriend) Create() {
    database.DB.Create(&chatUserFriend)
}

func (chatUserFriend *ChatUserFriend) Save() (rowsAffected int64) {
    result := database.DB.Save(&chatUserFriend)
    return result.RowsAffected
}

func (chatUserFriend *ChatUserFriend) Delete() (rowsAffected int64) {
    result := database.DB.Delete(&chatUserFriend)
    return result.RowsAffected
}

func (chatUserFriend *ChatUserFriend) DeleteAll(idstr string) (rowsAffected int64) {
    result := database.DB.Delete(&ChatUserFriend{}, strings.Split(idstr, ","))
	return result.RowsAffected
}