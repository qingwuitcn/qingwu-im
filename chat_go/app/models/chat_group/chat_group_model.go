// Package chat_group 模型
package chat_group

import (
	"pro/app/models"
	"pro/app/models/chat_group_user"
	"pro/pkg/database"
	"strings"
)

type ChatGroup struct {
    models.BaseModel

    // Put fields in here
	BelongId uint64 `json:"belong_id"`
	Name string `json:"name"`
    Avatar string `json:"avatar"`
	ChatGroupUser []chat_group_user.ChatGroupUserExt `json:"chat_group_users" gorm:"-"` 

    models.CommonTimestampsField
}



func (chatGroup *ChatGroup) Create() {
    database.DB.Create(&chatGroup)
}

func (chatGroup *ChatGroup) Save() (rowsAffected int64) {
    result := database.DB.Save(&chatGroup)
    return result.RowsAffected
}

func (chatGroup *ChatGroup) Delete() (rowsAffected int64) {
    result := database.DB.Delete(&chatGroup)
    return result.RowsAffected
}

func (chatGroup *ChatGroup) DeleteAll(idstr string) (rowsAffected int64) {
    result := database.DB.Delete(&ChatGroup{}, strings.Split(idstr, ","))
	return result.RowsAffected
}