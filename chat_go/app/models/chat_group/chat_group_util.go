package chat_group

import (
	"pro/pkg/database"
    "pro/app/requests"
	"pro/pkg/paginator"
	"strings"

	"github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

func Get(idstr string) (chatGroup ChatGroup) {
    database.DB.Where("id", idstr).First(&chatGroup)
    return
}

func GetIn(idstr string) (chatGroup []ChatGroup) {
	database.DB.Find(&chatGroup, strings.Split(idstr, ","))
	return
}

func GetBy(field, value string) (chatGroup ChatGroup) {
    database.DB.Where(field + " = ?" , value).First(&chatGroup)
    return
}

func All() (chatGroups []ChatGroup) {
    database.DB.Find(&chatGroups)
    return 
}

func Paginate(c *gin.Context, request *requests.ChatGroupRequest, perPage int) (chatGroups []ChatGroup, paging paginator.Paging) {
    model := SearchModel(request)
    paging = paginator.Paginate(
        c,
        model,
        &chatGroups,
        perPage,
    )
    return
}

func SearchModel(request *requests.ChatGroupRequest) *gorm.DB{
	model := database.DB.Model(ChatGroup{})
	// if !helpers.Empty(request.Name) {
	//	  model = model.Where("name like ?","%" + request.Name + "%")
	// }
	return model
}

func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(ChatGroup{}).Where(field + " = ?",  value).Count(&count)
    return count > 0
}


