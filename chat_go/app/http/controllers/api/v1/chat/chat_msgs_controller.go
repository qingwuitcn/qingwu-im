package v1

import (
	"pro/app/models/chat_group_user"
	"pro/app/models/chat_msg"
	"pro/app/models/chat_read_msg"
	"pro/app/models/chat_user_friend"
	"pro/app/requests"
	"pro/pkg/auth"
	"pro/pkg/helpers"
	"pro/pkg/response"

	"github.com/gin-gonic/gin"
)

type ChatMsgsController struct {
    BaseAPIController
}

func (ctrl *ChatMsgsController) Index(c *gin.Context) {
    // chatMsgs := chat_msg.All()
    // response.Data(c, chatMsgs)
    request := requests.ChatMsgRequest{}
	if ok := requests.Validate(c, &request, requests.ChatMsgPagination); !ok {
		return
	}
	uid := auth.CurrentUID(c)
	if !helpers.Empty(request.GroupId) {
		if isGroupMember := chat_group_user.IsGroupMember(uid,helpers.Uint64ToString(request.GroupId)); !isGroupMember {
			response.ErrMsg(c,"不是群成员")
			return 
		}
	}else{
		if uid != helpers.Uint64ToString(request.SendId) && uid != helpers.Uint64ToString(request.ReceiveId) {
			response.ErrMsg(c,"找不到用户")
			return 
		}
	}
	
	data, pager := chat_msg.Paginate(c, &request, 30)
	response.Pagination(c, data, pager)
}

func (ctrl *ChatMsgsController) Show(c *gin.Context) {
    chatMsgModel := chat_msg.Get(c.Param("id"))
    if chatMsgModel.ID == 0 {
        response.Abort404(c)
        return
    }
    response.Data(c, chatMsgModel)
}

// 系统消息申请通过设置
func (ctrl *ChatMsgsController) Update(c *gin.Context) {

    chatMsgModel := chat_msg.Get(c.Param("id"))
    if chatMsgModel.ID == 0 {
        response.Abort404(c)
        return
    }

    request := requests.ChatMsgRequest{}
    
    if ok := requests.Validate(c, &request, requests.ChatMsgPassAddFriend); !ok {
        return
    }

	if chatMsgModel.IsType != 6 && chatMsgModel.Code == "" {
		// 如果不是系统信息
		response.ErrMsg(c)
        return
	}

    chatMsgModel.PassType = request.PassType

	if !helpers.Empty(request.Content) {
		chatMsgModel.Content = request.Content
	}
    
    rowsAffected := chatMsgModel.Save()

	if request.PassType != 1 {
		response.Success(c)
		return
	}

	if chatMsgModel.Code == "add_friend" {
		isFriend := chat_user_friend.IsFriend(helpers.Uint64ToString(chatMsgModel.SendId),helpers.Uint64ToString(chatMsgModel.ReceiveId))
		if isFriend {
			// 已经是好友了
			response.ErrMsg(c)
			return
		}

		// 设置添加好友
		chatPassModel := chat_user_friend.ChatUserFriend{
			SendId: chatMsgModel.SendId,
			ReceiveId: chatMsgModel.ReceiveId,
		}

		chatPassModel.Save();
		if rowsAffected > 0 {
			response.Data(c,chatPassModel)
		} else {
			response.ErrMsg(c)
		}
	}

	if chatMsgModel.Code == "add_group" {
		isFriend := chat_group_user.GetByUserAndGroupExist(helpers.Uint64ToString(chatMsgModel.SendId),helpers.Uint64ToString(chatMsgModel.GroupId))
		if isFriend {
			// 已经是群成员了
			response.ErrMsg(c)
			return
		}

		// 设置添加群成员
		maxId := chat_group_user.GetMaxMapId(helpers.Uint64ToString(chatMsgModel.GroupId))
		chatPassModel := chat_group_user.ChatGroupUser{
			BelongId: chatMsgModel.SendId,
			GroupId: chatMsgModel.GroupId,
			MapId: maxId+1,
		}

		chatPassModel.Save();
		if rowsAffected > 0 {
			response.Data(c,chatPassModel)
		} else {
			response.ErrMsg(c)
		}
	}

}

// 获取系统信息的列表
func (ctrl *ChatMsgsController) SystemLog(c *gin.Context) {
	data,pager := chat_msg.GetSystemLog(c,auth.CurrentUID(c),30)
	// 需要清除所有未读信息
	response.Pagination(c, data, pager)
}

// 获取单个聊天用户信息
func (ctrl *ChatMsgsController) ChatingSigle(c *gin.Context) {
	request := requests.ChatMsgRequest{}
    if ok := requests.BindData(c, &request); !ok {
        return
    }
	uid := auth.CurrentUID(c)
	sendId := helpers.Uint64ToString(request.SendId)
	receiveId := helpers.Uint64ToString(request.ReceiveId)
	groupId := helpers.Uint64ToString(request.GroupId)
	isType := helpers.Uint64ToString(uint64(request.IsType))

	data := chat_msg.GetChatingSigle(uid,sendId,receiveId,isType,groupId)

	if request.GroupId > 0 {
		unread := chat_msg.GetGroupUnread(uid,groupId)
		data.Unread = uint64(unread)
	}else{
		// 判断我自己是否诗发送者
		if uid != sendId {
			unread := chat_msg.GetFriendUnread(receiveId,sendId)
			data.Unread = uint64(unread)
		}
	}
	
	response.Data(c, data)
}

// 修改好友间消息状态
func (ctrl *ChatMsgsController) UpdateUnreadFriend(c *gin.Context) {
	request := requests.ChatMsgRequest{}
    if ok := requests.BindData(c, &request); !ok {
        return
    }
	uid := auth.CurrentUID(c)
	sendId := helpers.Uint64ToString(request.SendId)
	chat_read_msg.UpdateUnreadFriend(sendId,uid)
	// 需要清除所有未读信息
	response.Success(c)
}


// 修改群消息间状态
func (ctrl *ChatMsgsController) UpdateUnreadGroup(c *gin.Context) {
	request := requests.ChatMsgRequest{}
    if ok := requests.BindData(c, &request); !ok {
        return
    }
	uid := auth.CurrentUID(c)
	gid := helpers.Uint64ToString(request.GroupId)
	// 获取用户
	groupMember := chat_group_user.GetByUserAndGroup(uid,gid)
	// 获取群最后一条消息
	groupLastMsg := chat_msg.GetBy("group_id",gid)
	if groupMember.ID > 0 {
		groupMember.LastReadMsgId = groupLastMsg.ID
		groupMember.Save()
	}
	response.Success(c)

}


