package v1

import (
	"pro/app/http/locales"
	"pro/app/models/chat_group"
	"pro/app/models/chat_group_user"
	"pro/app/models/chat_msg"
	"pro/app/models/chat_read_msg"
	"pro/app/models/chat_user"
	"pro/app/models/chat_user_friend"
	"pro/app/requests"
	"pro/pkg/auth"
	"pro/pkg/hash"
	"pro/pkg/helpers"
	"pro/pkg/jwt"
	"pro/pkg/response"

	"github.com/gin-gonic/gin"
)

type ChatUsersController struct {
	BaseAPIController
}

// 获取列表
func (ctrl *ChatUsersController) Index(c *gin.Context) {
	request := requests.ChatUserRequest{}
	if ok := requests.Validate(c, &request, requests.ChatUserPagination); !ok {
		return
	}
	data, pager := chat_user.Paginate(c, &request, 30)
	response.Pagination(c, data, pager)
}

// 显示
func (ctrl *ChatUsersController) Show(c *gin.Context) {
	chatUserModel := chat_user.Get(auth.CurrentUID(c))
	if chatUserModel.ID == 0 {
		response.Abort404(c)
		return
	}
	response.Data(c, chatUserModel)
}

func (ctrl *ChatUsersController) Update(c *gin.Context) {

    chatUserModel := chat_user.Get(c.Param("id"))
    if chatUserModel.ID == 0 {
        response.Abort404(c)
        return
    }

    request := requests.ChatUserRequest{}
    
    if ok := requests.BindData(c, &request); !ok {
        return
    }

	if !helpers.Empty(request.Nickname){
		chatUserModel.Nickname = request.Nickname
	}
	if !helpers.Empty(request.Avatar){
		chatUserModel.Avatar = request.Avatar
	}
    
    rowsAffected := chatUserModel.Save()
    if rowsAffected > 0 {
        response.Data(c, chatUserModel)
    } else {
        response.ErrMsg(c)
    }
}

// 用户登录
func (ctrl *ChatUsersController) Login(c *gin.Context) {
	UserType := "chatUser"
	request := requests.ChatUserRequest{}
	if ok := requests.Validate(c, &request, requests.ChatUserLogin); !ok {
		return
	}

	// 检测滑动验证签名
	// if !captcha.Check(request.Sign) {
	// 	response.ErrMsg(c, locales.T(c,"slideVerificationFailed"))
	// 	return
	// }

	chatUserModel := chat_user.GetByMulti(request.Username)

	if chatUserModel.ID == 0 {
		response.Abort500(c, locales.T(c,"accountIncorrect"))
		return
	} else {
		if checkPwd := chatUserModel.ComparePassword(request.Password); !checkPwd {
			response.Abort500(c, locales.T(c,"thePasswordIsIncorrect"))
			return
		}

		// 如果昵称为空则默认设置
		if helpers.Empty(chatUserModel.Nickname) {
			chatUserModel.Nickname = locales.T(c,"defaultNickname")
		}

		// 生成对应Token
		token := jwt.NewJWT().IssueToken(chatUserModel.GetStringID(), chatUserModel.Username, UserType)

		// 返回Token 数据
		response.Data(c, gin.H{
			"token": token,
			"nickname" : chatUserModel.Nickname,
			"username" : chatUserModel.Username,
			"msg":   "[" + chatUserModel.Nickname + "] "+locales.T(c,"welcomeToLogIn"),
		})
	}
}


// 注册
func (ctrl *ChatUsersController) Register(c *gin.Context) {
	request := requests.ChatUserRequest{}
	if ok := requests.Validate(c, &request, requests.ChatUserRegister); !ok {
		return
	}
	chatUserData := chat_user.GetByMulti(request.Username)

	if chatUserData.ID != 0 {
		response.Abort500(c, locales.T(c,"accountAlreadyExists"))
		return
	}

	if request.Password != request.RePassword {
		response.Abort500(c, locales.T(c,"passwordsNotMatch"))
		return
	}

	// 暂时先用username
	passwordDecode := hash.BcryptHash(request.Password)
	chatUserModel := chat_user.ChatUser{
		Username: request.Username,
		Phone:    request.Username,
		Nickname: request.Username,
		Password: passwordDecode,
	}
	rowsAffected := chatUserModel.Save()
	if rowsAffected > 0 {
		response.Data(c, chatUserModel)
	} else {
		response.ErrMsg(c)
	}

}

// 修改密码
func (ctrl *ChatUsersController) EditPassword(c *gin.Context){
	request := requests.ChatUserRequest{}
	if ok := requests.Validate(c, &request, requests.ChatUserEditPassword); !ok {
		return
	}
	chatUserModel := chat_user.Get(auth.CurrentUID(c))
	if(!chatUserModel.ComparePassword(request.OldPassword)){
		response.Abort500(c, locales.T(c,"oldPasswordError"))
		return
	}
	if request.Password != request.RePassword {
		response.Abort500(c, locales.T(c,"passwordsNotMatch"))
		return
	}
	passwordDecode := hash.BcryptHash(request.Password)
	chatUserModel.Password = passwordDecode
	rowsAffected := chatUserModel.Save()
	if rowsAffected > 0 {
		response.Data(c, chatUserModel)
	} else {
		response.ErrMsg(c)
	}
}


// 添加好友
func (ctrl *ChatUsersController) AddFriend(c *gin.Context){
	request := requests.ChatUserRequest{}
	if ok := requests.Validate(c, &request, requests.ChatUserSearch); !ok {
		return
	}
	uid := auth.CurrentUID(c)

	// 先去查询是否存在这么一个好友
	friendModel := chat_user.GetByMulti(request.Username)
	if friendModel.ID == 0 {
		// 不存在该用户
		response.ErrMsg(c,"不存在此用户")
		return
	}
	isFriend := chat_user_friend.IsFriend(uid,friendModel.GetStringID())
	if isFriend {
		// 已经是好友了
		response.ErrMsg(c,"已经是好友了")
		return
	}

	// 查询是否存在该申请
	if isExist := chat_msg.IsExistUnprocessed("add_friend","send_id",uid,friendModel.GetStringID()); isExist {
		// 已经已经申请
		response.ErrMsg(c,"已经已经申请")
		return
	}

	// 发送请求信息
	chatMsgModel := chat_msg.ChatMsg{
		SendId: helpers.ToNum(uid),
		ReceiveId: friendModel.ID,
		Code: "add_friend",
		IsType:    6,
	}
	
	chatMsgModel.Save()

	if chatMsgModel.ID > 0{
		// 插入一条读取数据
		chatReadMsg := chat_read_msg.ChatReadMsg{
			GroupId: 0,
			MsgId: chatMsgModel.ID,
		}
		chatReadMsg.Save()
		response.Data(c,friendModel.ID)
	}else{
		response.ErrMsg(c)
	}
}

// 申请入群
func (ctrl *ChatUsersController) AddGroup(c *gin.Context){
	request := requests.ChatUserRequest{}
	if ok := requests.Validate(c, &request, requests.ChatUserSearch); !ok {
		return
	}
	uid := auth.CurrentUID(c)

	// 先去查询是否存在这么一个好友
	groupModel := chat_group.GetBy("id",request.Username)
	if groupModel.ID == 0 {
		// 不存在此群
		response.ErrMsg(c,"不存在此群")
		return
	}
	isFriend := chat_group_user.GetByUserAndGroupExist(uid,groupModel.GetStringID())
	if isFriend {
		// 已经是此群成员了
		response.ErrMsg(c,"已经是此群成员了")
		return
	}

	// 查询是否存在该申请
	if isExist := chat_msg.IsExistUnprocessed("add_group","send_id",uid,groupModel.GetStringID()); isExist {
		// 已经已经申请
		response.ErrMsg(c,"已经已经申请")
		return
	}

	// 发送请求信息
	chatMsgModel := chat_msg.ChatMsg{
		GroupId: groupModel.ID,
		SendId: helpers.ToNum(uid),
		ReceiveId: groupModel.BelongId, // 发送给群管理员
		Code: "add_group",
		IsType:    6,
	}
	
	chatMsgModel.Save()

	if chatMsgModel.ID > 0{
		// 插入一条读取数据
		chatReadMsg := chat_read_msg.ChatReadMsg{
			GroupId: groupModel.ID,
			MsgId: chatMsgModel.ID,
		}
		chatReadMsg.Save()
		response.Data(c,groupModel.BelongId)
	}else{
		response.ErrMsg(c)
	}
}


func (ctrl *ChatUsersController) GetFriends(c *gin.Context){
	request := requests.ChatUserRequest{}
	if ok := requests.BindData(c, &request); !ok {
		return
	}
	uid := auth.CurrentUID(c)
	list := chat_msg.Friends(uid,request.Username)
	response.Data(c,list)
}


