package v1

import (
	"pro/app/http/locales"
	"pro/app/models/file_space"
	"pro/app/requests"
	"pro/pkg/auth"
	"pro/pkg/file"
	"pro/pkg/helpers"
	"pro/pkg/response"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

type FileSpacesController struct {
    BaseAPIController
}

func (ctrl *FileSpacesController) Index(c *gin.Context) {
    // fileSpaces := file_space.All()
    // response.Data(c, fileSpaces)
    request := requests.FileSpaceRequest{}
	if ok := requests.Validate(c, &request, requests.FileSpacePagination); !ok {
		return
	}
	data, pager := file_space.Paginate(c, &request, 30)
	response.Pagination(c, data, pager)
}

func (ctrl *FileSpacesController) Show(c *gin.Context) {
    fileSpaceModel := file_space.Get(c.Param("id"))
    if fileSpaceModel.ID == 0 {
        response.Abort404(c)
        return
    }
    response.Data(c, fileSpaceModel)
}

// 上传图片
func (ctrl *FileSpacesController) Image(c *gin.Context) {
	
	request := requests.FileSpaceRequest{}
	    if ok := requests.Validate(c, &request, requests.FileSpaceImageSave); !ok {
        return
    }
	fileResource, err := file.SaveUpload(c, request)
    if err != nil {
        response.Error(c,err, locales.T(c,"failedToUploadImage"))
        return
    }

    fileSpaceModel := file_space.FileSpace{
		BelongId: cast.ToUint64(auth.CurrentUID(c)),
		Name: fileResource.OldFileName,
		NewName: fileResource.FileName,
		Md5: fileResource.Md5,
		ExtName: fileResource.Ext,
		Url: fileResource.Url,
		DiskName:fileResource.DiskName ,
    }

	if !helpers.Empty(request.DirId) {
		fileSpaceModel.DirId = request.DirId
	}

    fileSpaceModel.Create()

    if fileSpaceModel.ID > 0 {
		resData := file_space.FileResource{
			Url: fileResource.Url,
			Md5: fileResource.Md5,
			Name: fileResource.OldFileName,
			NewName: fileResource.FileName,
			ExtName: fileResource.Ext,
			DiskName: fileResource.DiskName,
		}
        response.Data(c, resData)
    } else {
        response.ErrMsg(c)
    }
}
// 上传文件
func (ctrl *FileSpacesController) File(c *gin.Context) {
	request := requests.FileSpaceRequest{}
	    if ok := requests.Validate(c, &request, requests.FileSpaceFileSave); !ok {
        return
    }

	fileResource, err := file.SaveUpload(c, request)
    if err != nil {
        response.ErrMsg(c, locales.T(c,"uploadFileFailed"))
        return
    }

	resData := file_space.FileResource{
		Url: fileResource.Url,
		Md5: fileResource.Md5,
		Name: fileResource.OldFileName,
		NewName: fileResource.FileName,
		ExtName: fileResource.Ext,
		DiskName: fileResource.DiskName,
	}

	response.Data(c, resData)
}


func (ctrl *FileSpacesController) Update(c *gin.Context) {

    fileSpaceModel := file_space.Get(c.Param("id"))
    if fileSpaceModel.ID == 0 {
        response.Abort404(c)
        return
    }

    request := requests.FileSpaceRequest{}
    
    if ok := requests.Validate(c, &request, requests.FileSpaceSave); !ok {
        return
    }

    fileSpaceModel.Name = request.Name
    rowsAffected := fileSpaceModel.Save()
    if rowsAffected > 0 {
        response.Data(c, fileSpaceModel)
    } else {
        response.ErrMsg(c)
    }
}

func (ctrl *FileSpacesController) Delete(c *gin.Context) {

    fileSpaceModel := file_space.GetIn(c.Param("id"))
    if len(fileSpaceModel) == 0 {
        response.Abort404(c)
        return
    }

    rowsAffected := fileSpaceModel[0].DeleteAll(c.Param("id"))
	if rowsAffected > 0 {
		response.Success(c)
		return
	}

	response.ErrMsg(c)
}