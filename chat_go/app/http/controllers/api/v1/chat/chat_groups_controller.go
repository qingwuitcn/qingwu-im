package v1

import (
	"fmt"
	"pro/app/models/chat_group"
	"pro/app/models/chat_group_user"
	"pro/app/models/chat_user"
	"pro/app/models/chat_user_friend"
	"pro/app/requests"
	"pro/pkg/auth"
	"pro/pkg/helpers"
	"pro/pkg/response"
	"strings"

	"github.com/gin-gonic/gin"
)

type ChatGroupsController struct {
    BaseAPIController
}

func (ctrl *ChatGroupsController) Index(c *gin.Context) {
    // chatGroups := chat_group.All()
    // response.Data(c, chatGroups)
    request := requests.ChatGroupRequest{}
	if ok := requests.Validate(c, &request, requests.ChatGroupPagination); !ok {
		return
	}
	data, pager := chat_group.Paginate(c, &request, 30)
	response.Pagination(c, data, pager)
}

func (ctrl *ChatGroupsController) Show(c *gin.Context) {
    chatGroupModel := chat_group.Get(c.Param("id"))
    if chatGroupModel.ID == 0 {
        response.Abort404(c)
        return
    }

	// 获取群成员信息
	chatGroupUserModel := chat_group_user.AllByJoinUser("group_id",chatGroupModel.GetStringID())
	chatGroupModel.ChatGroupUser = chatGroupUserModel

    response.Data(c, chatGroupModel)
}

func (ctrl *ChatGroupsController) Store(c *gin.Context) {

    request := requests.ChatGroupRequest{}
    if ok := requests.Validate(c, &request, requests.ChatGroupSave); !ok {
        return
    }

	uid := auth.CurrentUID(c)
	memberId := strings.Split(request.MemberId,",")
	newMemberId := make([]string, len(memberId)+1)
	newMemberId[0] = uid
	copy(newMemberId[1:],memberId)
    chatGroupModel := chat_group.ChatGroup{
        BelongId:      helpers.ToNum(uid),
        Name:      request.Name,
        Avatar:      request.Avatar,
    }

	var friendMember []uint64
	for _,item := range newMemberId {
		if isFriend := chat_user_friend.IsFriend(uid,item); isFriend || item == uid {
			friendMember = append(friendMember, helpers.ToNum(item))
		}
	}
	if len(friendMember) <= 0 {
		fmt.Println("您的群成员:",friendMember,newMemberId)
		response.ErrMsg(c,"请选择您的群成员")
		return
	}
    chatGroupModel.Create()
	
    if chatGroupModel.ID > 0 {
		var chatGroupUser []chat_group_user.ChatGroupUser
		for index,item := range friendMember {
			chatGroupUser = append(chatGroupUser, chat_group_user.ChatGroupUser{
				BelongId: item,
				MapId: uint64(index),
				GroupId:chatGroupModel.ID ,
			})
		}
		chat_group_user.BatchCreate(chatGroupUser)
        response.Data(c,chatGroupModel)
    } else {
        response.ErrMsg(c)
    }
}

func (ctrl *ChatGroupsController) Update(c *gin.Context) {

    chatGroupModel := chat_group.Get(c.Param("id"))
    if chatGroupModel.ID == 0 {
        response.Abort404(c)
        return
    }

    request := requests.ChatGroupRequest{}
    
    if ok := requests.BindData(c, &request); !ok {
        return
    }

	// 判断是否属于这个用户
	uid := auth.CurrentUID(c)

	if !helpers.Empty(request.Name){
		if chatGroupModel.BelongId == helpers.ToNum(uid){
			chatGroupModel.Name = request.Name
		}
	}
	if !helpers.Empty(request.Avatar){
		if chatGroupModel.BelongId == helpers.ToNum(uid){
			chatGroupModel.Avatar = request.Avatar
		}
	}

	if !helpers.Empty(request.MemberId){
		// 判断增加用户是否是这个群内成员
		isExist := chat_group_user.GetByUserAndGroupExist(uid,chatGroupModel.GetStringID())
		var addMemberId []string
		if isExist {
			memberIds := strings.Split(request.MemberId,",")
			maxMapId := chat_group_user.GetMaxMapId(chatGroupModel.GetStringID())
			for _,item := range memberIds {
				if userExist := chat_group_user.GetByUserAndGroupExist(item,chatGroupModel.GetStringID()); !userExist {
					maxMapId += 1
					addMemberId = append(addMemberId, item)
					chatGroupUserModel := chat_group_user.ChatGroupUser{
						BelongId: helpers.ToNum(item),
						GroupId: chatGroupModel.ID,
						MapId: maxMapId,
					}
					chatGroupUserModel.Save()
					
				}
			}
		}
		// 查询到所有的用户信息
		userList := chat_user.GetIn(strings.Join(addMemberId, ","))
		var userNickname [] string 
		if len(userList) > 0{
			for _,item := range userList {
				userNickname = append(userNickname, item.Nickname)
			}
		}
		rowsAffected := chatGroupModel.Save()
		if rowsAffected > 0 {
			response.Data(c, userNickname)
		} else {
			response.ErrMsg(c)
		}
		return
	}
    
    rowsAffected := chatGroupModel.Save()
    if rowsAffected > 0 {
        response.Data(c, chatGroupModel)
    } else {
        response.ErrMsg(c)
    }
}

func (ctrl *ChatGroupsController) Delete(c *gin.Context) {

    chatGroupModel := chat_group.GetIn(c.Param("id"))
    if len(chatGroupModel) == 0 {
        response.Abort404(c)
        return
    }

	uid := auth.CurrentUID(c)
	for _,item := range chatGroupModel {
		if item.BelongId != helpers.ToNum(uid) {
			response.ErrMsg(c)
			return 
		}
	} 

	// 删除所有群用户
	// chat_group_user.DeleteAllByGroup(c.Param("id"))

    rowsAffected := chatGroupModel[0].DeleteAll(c.Param("id"))
	if rowsAffected > 0 {
		response.Success(c)
		return
	}

	response.ErrMsg(c)
}