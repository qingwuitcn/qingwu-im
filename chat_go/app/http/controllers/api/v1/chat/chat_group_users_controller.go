package v1

import (
	"pro/app/models/chat_group"
	"pro/app/models/chat_group_user"

	"pro/app/requests"
	"pro/pkg/auth"
	"pro/pkg/helpers"
	"pro/pkg/response"

	"github.com/gin-gonic/gin"
)

type ChatGroupUsersController struct {
    BaseAPIController
}

// func (ctrl *ChatGroupUsersController) Index(c *gin.Context) {
//     // chatGroupUsers := chat_group_user.All()
//     // response.Data(c, chatGroupUsers)
//     request := requests.ChatGroupUserRequest{}
// 	if ok := requests.Validate(c, &request, requests.ChatGroupUserPagination); !ok {
// 		return
// 	}
// 	data, pager := chat_group_user.Paginate(c, &request, 30)
// 	response.Pagination(c, data, pager)
// }

// func (ctrl *ChatGroupUsersController) Show(c *gin.Context) {
//     chatGroupUserModel := chat_group_user.Get(c.Param("id"))
//     if chatGroupUserModel.ID == 0 {
//         response.Abort404(c)
//         return
//     }
//     response.Data(c, chatGroupUserModel)
// }

// func (ctrl *ChatGroupUsersController) Store(c *gin.Context) {

//     request := requests.ChatGroupUserRequest{}
//     if ok := requests.Validate(c, &request, requests.ChatGroupUserSave); !ok {
//         return
//     }

//     chatGroupUserModel := chat_group_user.ChatGroupUser{
//         FieldName:      request.FieldName,
//     }
//     chatGroupUserModel.Create()
//     if chatGroupUserModel.ID > 0 {
//         response.Success(c)
//     } else {
//         response.ErrMsg(c)
//     }
// }

// func (ctrl *ChatGroupUsersController) Update(c *gin.Context) {

//     chatGroupUserModel := chat_group_user.Get(c.Param("id"))
//     if chatGroupUserModel.ID == 0 {
//         response.Abort404(c)
//         return
//     }

//     request := requests.ChatGroupUserRequest{}
    
//     if ok := requests.Validate(c, &request, requests.ChatGroupUserSave); !ok {
//         return
//     }

//     chatGroupUserModel.FieldName = request.FieldName
//     rowsAffected := chatGroupUserModel.Save()
//     if rowsAffected > 0 {
//         response.Data(c, chatGroupUserModel)
//     } else {
//         response.ErrMsg(c)
//     }
// }

func (ctrl *ChatGroupUsersController) DeleteUser(c *gin.Context) {

	request := requests.ChatGroupUserRequest{}
	if ok := requests.BindData(c, &request); !ok {
		return
	}
	uid := auth.CurrentUID(c)
	belongId := helpers.Uint64ToString(request.BelongId)
	gid := helpers.Uint64ToString(request.GroupId)
    chatGroupUserModel := chat_group_user.GetByUserAndGroup(belongId,gid)
    if chatGroupUserModel.ID == 0 {
		// 查询是否是管理，如果是管理也可以删除
		chatGroupModel := chat_group.Get(gid)
		if chatGroupModel.BelongId == helpers.ToNum(uid) {
			response.Abort404(c)
        	return
		}
    }

    rowsAffected := chatGroupUserModel.Delete()
	if rowsAffected > 0 {
		response.Success(c)
		return
	}

	response.ErrMsg(c)
}

func (ctrl *ChatGroupUsersController) Delete(c *gin.Context) {

	uid := auth.CurrentUID(c)
	gid := c.Param("id")
    chatGroupUserModel := chat_group_user.GetByUserAndGroup(uid,gid)
    // if chatGroupUserModel.ID == 0 {
	// 	// 查询是否是管理，如果是管理也可以删除
	// 	chatGroupModel := chat_group.Get(gid)
	// 	if chatGroupModel.BelongId == helpers.ToNum(uid) {
	// 		response.Abort404(c)
    //     	return
	// 	}
    // }

    rowsAffected := chatGroupUserModel.Delete()
	if rowsAffected > 0 {
		response.Success(c)
		return
	}

	response.ErrMsg(c)
}
