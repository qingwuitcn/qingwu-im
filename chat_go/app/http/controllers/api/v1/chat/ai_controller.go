package v1

import (
	"context"
	"fmt"
	"io"
	"pro/app/models/chat_ai_msg"
	"pro/app/requests"
	"pro/pkg/auth"
	"pro/pkg/helpers"
	"pro/pkg/logger"
	"pro/pkg/response"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tmc/langchaingo/llms"
	// "github.com/tmc/langchaingo/memory"
	"github.com/tmc/langchaingo/llms/ollama"
)

type AiController struct {
	BaseAPIController
}


func (ctrl *AiController) Ai(c *gin.Context) {
	request := requests.AiRequest{}
	if ok := requests.Validate(c, &request, requests.AiSave); !ok {
		return
	}
	
	var receiveId uint64 = 1
	if !helpers.Empty(request.ReceiveId){
		receiveId = request.ReceiveId
	}

	uid := auth.CurrentUID(c)
	// 创建模型数据
	chatAiModel := chat_ai_msg.ChatAiMsg{
		SendId: helpers.ToNum(uid),
		ReceiveId: receiveId,
		Code: helpers.RandomString(18),
		Content:request.Prompt,
	}

	chatAiModel.Save()

	if chatAiModel.ID > 0 {
		response.Data(c,chatAiModel.Code)
	}else{
		response.ErrMsg(c)
	}
}

func (ctrl *AiController) SseAi(c *gin.Context) {
	// 设置Content-Type为text/event-stream，并且允许跨域
	c.Header("Content-Type", "text/event-stream")
	c.Header("Cache-Control", "no-cache")
	c.Header("Connection", "keep-alive")
	c.Header("Access-Control-Allow-Origin", "*")

	request := requests.AiRequest{}
	if ok := requests.Validate(c, &request, requests.SseAiSave); !ok {
		return
	}

	// 获取聊天信息
	chatAiMsgModel := chat_ai_msg.GetBy("code",request.Code)

	// 如果是超时请求则直接返回错误
	tl := time.Time(*chatAiMsgModel.CreatedAt)
	nowTime := time.Now()
	duration := nowTime.Sub(tl)
	if duration.Seconds() > 40 {
		response.ErrMsg(c)
		return
	}

	// 创建一个执行结束
	taskCh := make(chan []byte)
	endCh := make(chan string)
	defer close(taskCh)
	defer close(endCh)
	go handleOllama(chatAiMsgModel,taskCh,endCh)
	// go waitAiRes(c,taskCh,endCh)
	
	
	c.Stream(func(w io.Writer) bool {
		select {
		case taskMsg, ok := <-taskCh:
			if !ok {
				fmt.Println("ch1 已关闭")
				break
			}
			// 获取AI回答插入数据库
			chatAiMsgModel.AiContent += string(taskMsg)
			htmlMsg := strings.ReplaceAll(string(taskMsg), "\n", "<br>")
			if _, err := fmt.Fprintf(w, "data: %s\n\n", htmlMsg); err != nil {
				// 写入错误，结束流
				return false
			}
			// fmt.Println("接收到 taskCh 的数据:", taskMsg)
		case endMsg, ok := <-endCh:
			if !ok {
				fmt.Println("ch2 已关闭")
				break
			}
			// 接收到数据结束
			fmt.Println("接收到 endCh 的数据:", endMsg)
			if endMsg == "end" {
				chatAiMsgModel.Save() // 保存AI作答
				return false
			}
		}
		return true
	})
	
}

// 第一个任务通道，第二个结束通道
// func waitAiRes(c *gin.Context,taskCh,endCh chan string){
// 	defer close(taskCh)
// 	defer close(endCh)
// 	c.Stream(func(w io.Writer) bool {
// 		for {
// 			select {
// 			case taskMsg, ok := <-taskCh:
// 				if !ok {
// 					fmt.Println("ch1 已关闭")
// 					break
// 				}
// 				if _, err := fmt.Fprintf(w, "data: %s\n\n", taskMsg); err != nil {
// 					// 写入错误，结束流
// 					return false
// 				}
// 				fmt.Println("接收到 taskCh 的数据:", taskMsg)
// 			case endMsg, ok := <-endCh:
// 				if !ok {
// 					fmt.Println("ch2 已关闭")
// 					break
// 				}
// 				// 接收到数据结束
// 				fmt.Println("接收到 endCh 的数据:", endMsg)
// 				if endMsg == "end" {
// 					return false
// 				}
// 			}
// 		}
// 	})
// }

func handleOllama (chatAiMsgModel chat_ai_msg.ChatAiMsg,taskCh chan []byte,endCh chan string){
	defer func() {
        if r := recover(); r != nil {
            fmt.Printf("Recovered in handleOllama: %v\n", r)
        }
    }()
	aiModel := []string{"","qwen2:0.5b","qwen2:1.5b","qwen2:7b"}
	
	llm, err := ollama.New(ollama.WithModel(aiModel[chatAiMsgModel.ReceiveId]))
	if err != nil {
        logger.DebugString(aiModel[chatAiMsgModel.ReceiveId],"ai",err.Error())
        logger.DebugString(aiModel[chatAiMsgModel.ReceiveId],"ai",chatAiMsgModel.Content)
		return
    }
	ctx := context.Background()
	// fmt.Println( "开始时间" + time.Now().Format(time.StampMilli))
	fmt.Printf("用户【%d】发送提示词:%s \n\r", chatAiMsgModel.SendId,chatAiMsgModel.Content)

	// 获取10条上下文
	content := []llms.MessageContent{}
	chatAiMsgList := chat_ai_msg.AllContextByLimit(helpers.Uint64ToString(chatAiMsgModel.SendId),helpers.Uint64ToString(chatAiMsgModel.ReceiveId),10)
	// 查出来的数据倒叙
	for i, j := 0, len(chatAiMsgList)-1; i < j; i, j = i+1, j-1 {
        chatAiMsgList[i], chatAiMsgList[j] = chatAiMsgList[j], chatAiMsgList[i]
    }
	
	for _,item := range chatAiMsgList {

		if !helpers.Empty(item.Content){
			content = append(content, llms.MessageContent{
				Role: llms.ChatMessageTypeHuman,
				Parts:[]llms.ContentPart{
					llms.TextContent{Text: item.Content},
				},
			})
		}
		if !helpers.Empty(item.AiContent){
			content = append(content, llms.MessageContent{
				Role: llms.ChatMessageTypeAI,
				Parts:[]llms.ContentPart{
					llms.TextContent{Text: item.AiContent},
				},
			})
		}
	}
	content = append(content, llms.MessageContent{
		Role: llms.ChatMessageTypeHuman,
		Parts:[]llms.ContentPart{
			llms.TextContent{Text: chatAiMsgModel.Content},
		},
	})

	completion, err := llm.GenerateContent(ctx, content,
        llms.WithTemperature(1),
        llms.WithStreamingFunc(func(ctx context.Context, chunk []byte) error {
			// fmt.Print(chunk)
			// c.SSEvent("message",string(chunk))
			taskCh <- chunk
            return nil
        }),
    )
    if err != nil {
        logger.DebugString("SSE AI","ai",err.Error())
		return
    }
	
	endCh <- "end"
	_ = completion
}

