package v1

import (
	"pro/app/models/chat_user_friend"
	"pro/app/requests"
	"pro/pkg/auth"
	"pro/pkg/helpers"
	"pro/pkg/response"

	"github.com/gin-gonic/gin"
)

type ChatUserFriendsController struct {
    BaseAPIController
}



func (ctrl *ChatUserFriendsController) DeleteFriend(c *gin.Context) {
	
	request := requests.ChatUserFriendRequest{}
	if ok := requests.Validate(c, &request, requests.ChatUserFriendSave); !ok {
		return
	}
	uid := auth.CurrentUID(c)
    chatUserFriendModel := chat_user_friend.GetFriend(uid,helpers.Uint64ToString(request.FriendId))

    rowsAffected := chatUserFriendModel.Delete()
	if rowsAffected > 0 {
		response.Success(c)
		return
	}

	response.ErrMsg(c)
}