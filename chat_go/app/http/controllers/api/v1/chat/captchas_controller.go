package v1

import (
	"pro/app/models/captcha"
	"pro/app/requests"
	"pro/pkg/config"
	"pro/pkg/helpers"
	"pro/pkg/response"

	"github.com/gin-gonic/gin"
)

type CaptchasController struct {
    BaseAPIController
}


// 创建滑动链接参数
func (ctrl *CaptchasController) Store(c *gin.Context) {

	request := requests.CaptchaRequest{}
	if ok := requests.Validate(c, &request, requests.CaptchaSave); !ok {
		return
	}

	// 先查询
	requestState := captcha.CountByIp(c.ClientIP(),10)
	if requestState {
		response.Abort429(c)
		return
	}

	captchaModel := captcha.Captcha{
		Username: request.Username,
		PrivateKey:helpers.RandomString(8), // 生成随机数
		Captcha:helpers.RandomNum(40, 80),
		Ip:c.ClientIP(),
	}
	appKey := config.GetString("VUE_KEY","QINGWUIT")
	captchaModel.Sign = helpers.MakeMd5(captchaModel.PrivateKey + captchaModel.Username + captchaModel.Captcha + appKey)

	captchaModel.Create()

	if captchaModel.ID > 0 {
		response.Data(c, captchaModel)
	} else {
		response.ErrMsg(c)
	}
}

