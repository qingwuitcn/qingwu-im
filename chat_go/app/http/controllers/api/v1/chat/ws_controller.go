package v1

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	// "pro/pkg/auth"
	"pro/app/models/chat_group_user"
	"pro/app/models/chat_msg"
	"pro/app/models/chat_read_msg"
	"pro/app/models/chat_user_friend"
	"pro/pkg/helpers"
	"pro/pkg/jwt"
	"pro/pkg/logger"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type WsController struct {
	BaseAPIController
}

// Token data
var jwtStruct *jwt.JWT

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
} 
// 连接服务端Map
var wsConn map[string][]*websocket.Conn = make(map[string][]*websocket.Conn, 0)

// 读写锁锁
var rwLock sync.RWMutex

// 消息结构体
type WsMessage struct {
	Token        string `json:"token"`      // 密钥
	Code         string `json:"code"`       // 类型
	GroupId      uint64 `json:"group_id"`   // 群ID
	SendId       uint64 `json:"send_id"`    // 发送人
	ReceiveId    uint64 `json:"receive_id"` // 接收人
	IsType       uint8  `json:"is_type"`    // 类型1文字-2图片-3文件-4分享-5语音-6系统消息-7接口请求"
	Content      string `json:"content"`    // 内容
	ShowDiffTime uint8  `json:"show_diff_time"`
}

// set Token Data
// func setTokenData(data *jwt.JWTCustomClaims){
// 	JWTCustomClaims = data
// }

// 创建滑动链接参数
func (ctrl *WsController) Websocket(c *gin.Context) {
	jwtStruct = jwt.NewJWT() // 初始化解析token

	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()

	// 获取用户信息
	conn.SetCloseHandler(func(code int, text string) error {
		logger.DebugString("websocket", "ws", fmt.Sprintf("连接关闭，代码: %d, 原因: %s\n", code, text))
		return nil
	})

	// go Heartbeat(conn) // 心跳
	for {
		// 读取客户端发送的消息
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			logger.ErrorJSON("websocket", "ReadMessage", err.Error())
			return
		}
		go ReceiveMessage(conn, messageType, p)
	}

}

// 接收ws客户端传输信息
func ReceiveMessage(conn *websocket.Conn, messageType int, data []byte) {

	defer func() {
        if r := recover(); r != nil {
            fmt.Printf("Recovered in ReceiveMessage: %v\n", r)
        }
    }()
    
	// 获取消息 解析消息
	var wsMsg WsMessage
	err := json.Unmarshal(data, &wsMsg)

	if err != nil {
		logger.ErrorString("websocket", "Parse", string(data))
		logger.ErrorJSON("websocket", "Parse", err.Error())
		return
	}
	JWTCustomClaims, error := jwtStruct.ParserTokenByString(wsMsg.Token)
	if error != nil {
		logger.DebugString("websocket", "ws", "[token] 解析失败-"+error.Error())
		wsMsg.Code = "auth_error"
		jsonBytes, err := json.Marshal(wsMsg)
		if err != nil {
			logger.ErrorJSON("websocket", "auth_error", err.Error())
		}
		conn.WriteMessage(messageType, jsonBytes)
		conn.Close()
		return
	}

	uid := JWTCustomClaims.UserID
	wsMsg.SendId = helpers.ToNum(uid)
	wsMsg.Token = ""

	// 绑定id
	if wsMsg.Code == "bind_user" {
		rwLock.Lock()
		if _,ok := wsConn[uid]; !ok{
			wsConn[uid] = []*websocket.Conn{}
		}
		wsConn[uid] = append(wsConn[uid], conn)
		rwLock.Unlock()

		// 获取设备数量
		var deviceSum int = 0
		for _,connItem := range wsConn {
			deviceSum += len(connItem)
		}

		// 绑定的时候建立心跳,这个心跳很重要，关乎wsConn维护清理
		go Heartbeat(uid,conn) // 心跳

		// 创建一个心跳
		fmt.Printf("客户:%d 个,客户端连接数：%d  台 \n\r", len(wsConn),deviceSum)
		return
	}

	// 获取聊天列表
	if wsMsg.Code == "get_chat_lists" {
		chats := chat_msg.GetChatingList(uid,wsMsg.Content)
		if len(chats) > 0 {
			for index, v := range chats {
				// 2图片-3文件-4分享-5语音-6系统消息-7接口请求
				// if v.IsType == 1 && len(v.Content) > 20 {
				// 	chats[index].Content = v.Content[0:20]
				// }
				if v.IsType == 2 {
					chats[index].Content = "[图片]"
				}
				if v.IsType == 3 {
					chats[index].Content = "[文件]"
				}
				if v.IsType == 4 {
					chats[index].Content = "[分享]"
				}
				if v.IsType == 5 {
					chats[index].Content = "[语音]"
				}
				// if v.IsType == 7 {
				// 	chats[index].Content = "[系统消息]"
				// }
				// // 如果是群信息读取未读信息  这里后面可以分开发送给客户，用redis队列处理
				if v.GroupId > 0 && v.Code == "" {
					unreadCount := chat_msg.GetGroupUnread(uid, helpers.Uint64ToString(v.GroupId))
					chats[index].Unread = uint64(unreadCount)
				} else {
					unreadCount := chat_msg.GetFriendUnread(uid, helpers.Uint64ToString(v.SendId))
					chats[index].Unread = uint64(unreadCount)
				}
			}
		}
		chating, err := json.Marshal(chats)
		if err != nil {
			logger.ErrorJSON("websocket", "get_chat_lists", err.Error())
		}
		wsMsg.Content = string(chating)
		jsonBytes, err := json.Marshal(wsMsg)
		if err != nil {
			logger.ErrorJSON("websocket", "get_chat_lists2", err.Error())
		}
		SendMessage(uid, messageType, jsonBytes)
		return
	}

	// 获取通讯录
	if wsMsg.Code == "get_friends" {
		friends, err := json.Marshal(chat_msg.Friends(uid,wsMsg.Content))
		if err != nil {
			logger.ErrorJSON("websocket", "get_friends", err.Error())
		}
		wsMsg.Content = string(friends)
		jsonBytes, err := json.Marshal(wsMsg)
		if err != nil {
			logger.ErrorJSON("websocket", "get_friends2", err.Error())
		}
		SendMessage(uid, messageType, jsonBytes)
		return
	}

	// 获取通讯录
	if wsMsg.Code == "get_groups" {
		groups, err := json.Marshal(chat_msg.Groups(uid,wsMsg.Content))
		if err != nil {
			logger.ErrorJSON("websocket", "get_groups", err.Error())
		}
		wsMsg.Content = string(groups)
		jsonBytes, err := json.Marshal(wsMsg)
		if err != nil {
			logger.ErrorJSON("websocket", "get_groups2", err.Error())
		}
		SendMessage(uid, messageType, jsonBytes)
		return
	}

	// 如果是删除用户则通知给好友
	if wsMsg.Code == "friend_leave" {
		jsonBytes, err := json.Marshal(wsMsg)
		if err != nil {
			logger.ErrorJSON("websocket", "friend_leave", err.Error())
		}
		SendMessage(helpers.Uint64ToString(wsMsg.ReceiveId), messageType, jsonBytes)
	}

	// 如果是管理员删除群成员
	if wsMsg.Code == "leader_delete" {
		jsonBytes, err := json.Marshal(wsMsg)
		if err != nil {
			logger.ErrorJSON("websocket", "friend_leave", err.Error())
		}
		SendMessage(wsMsg.Content, messageType, jsonBytes)
	}

	// 如果是管理员解散群
	if wsMsg.Code == "drop_group" {
		// 除了自己的所有群成员
		groupIds := chat_group_user.GetGroupUserId(helpers.Uint64ToString(wsMsg.GroupId))
		// 删除所有群成员
		chat_group_user.DeleteAllByGroup(helpers.Uint64ToString(wsMsg.GroupId))
		if len(groupIds) > 0 {
			for _,gourpItem := range groupIds {
				jsonBytes, err := json.Marshal(wsMsg)
				if err != nil {
					logger.ErrorJSON("websocket", "friend_leave", err.Error())
				}
				SendMessage(helpers.Uint64ToString(gourpItem), messageType, jsonBytes)
			}
		}
		
	}

	// 添加好友申请/群申请
	if wsMsg.Code == "add_friend" || wsMsg.Code == "add_group" {
		wsMsg.Code = ""
		jsonBytes, err := json.Marshal(wsMsg)
		if err != nil {
			logger.ErrorJSON("websocket", "add_friend/add_group", err.Error())
		}
		SendMessage(helpers.Uint64ToString(wsMsg.ReceiveId), messageType, jsonBytes)
	}

	// 发送消息
	if wsMsg.Code == "send" {
		SendMessageHandle(wsMsg, messageType)
	}

}

// 发送消息
func SendMessage(uid string, messageType int, data []byte) {
	if len(wsConn[uid]) > 0 {
		for _,conn := range wsConn[uid] {
			go ThSendMessage(conn,messageType,data)
		}
	}
	
}

func ThSendMessage(conn *websocket.Conn, messageType int, data []byte){
	defer func() {
        if r := recover(); r != nil {
            fmt.Printf("Recovered in ThSendMessage: %v\n", r)
        }
    }()
	err := conn.WriteMessage(messageType, data)
	if err != nil {
		logger.ErrorJSON("websocket", "sendMessage", err.Error())
	}
}

func Heartbeat(uid string,cnn *websocket.Conn) {
	defer func() {
        if r := recover(); r != nil {
            fmt.Printf("Recovered in Heartbeat: %v\n", r)
        }
    }()
	ticker := time.NewTicker(time.Second * 5)
	defer ticker.Stop()
	for range ticker.C {
		err := cnn.WriteControl(websocket.PingMessage, []byte("heartbeat"), time.Now().Add(time.Second))
		if err != nil {
			logger.ErrorJSON("websocket", "close", fmt.Sprintf("连接断开: %s", uid))
			logger.ErrorJSON("websocket", "Heartbeat", fmt.Sprintf("write ping error: %v", err))
			// 失败时去map中找到对应的自己，并关闭连接
			// 判断这个是否存在
			if _,ok := wsConn[uid]; !ok {
				return
			}
			if len(wsConn[uid]) > 0 {
				for index,connItem := range wsConn[uid] {
					if(connItem == cnn){
						wsConn[uid] = append(wsConn[uid][:index], wsConn[uid][index+1:]...)
					}
				}
				// 再查询wsConn长度如果小于0 则直接删除
				if len(wsConn[uid]) == 0 {
					delete(wsConn, uid)
				}
			}
			return
		}
	}
}

// 发送消息处理
func SendMessageHandle(wsMsg WsMessage, messageType int) {
	// 判断是否是空消息
	if helpers.Empty(wsMsg.Content) {
		return
	}

	sendId := helpers.Uint64ToString(wsMsg.SendId)
	receiveId := helpers.Uint64ToString(wsMsg.ReceiveId)
	groupId := helpers.Uint64ToString(wsMsg.GroupId)

	var ShowDiffTime uint8 = 0 // 是否显示距离当前时间 默认不显示下面可以判断查询最近一次聊天是否在五分钟前

	// 判断两人是否是好友
	if wsMsg.GroupId == 0 {
		if isFriend := chat_user_friend.IsFriend(sendId, receiveId); !isFriend {
			fmt.Println("两人不是好友无法发送信息",sendId,receiveId)
			return
		}
	}

	lastMsgModel := chat_msg.GetLastMsg(sendId,receiveId,groupId)
	if lastMsgModel.ID > 0 {
		tl := time.Time(*lastMsgModel.CreatedAt)
		nowTime := time.Now()
		duration := nowTime.Sub(tl)
		if duration.Minutes() > 5 {
			ShowDiffTime = 1
		}
	}

	// 需要存储消息
	chatMsg := chat_msg.ChatMsg{
		GroupId:      wsMsg.GroupId,
		SendId:       wsMsg.SendId,
		ReceiveId:    wsMsg.ReceiveId,
		Code:         "", // 那边接收消息
		IsType:       wsMsg.IsType,
		Content:      wsMsg.Content,
		ShowDiffTime: ShowDiffTime,
	}
	chatMsg.Save()

	// 未读维护字段
	chatReadMsg := chat_read_msg.ChatReadMsg{
		MsgId: chatMsg.ID,
	}
	chatReadMsg.Save()

	wsMsg.Code = ""
	wsMsg.ShowDiffTime = ShowDiffTime
	jsonBytes, err := json.Marshal(wsMsg)
	if err != nil {
		logger.ErrorJSON("websocket", "SendMessageHandle Marshal", err.Error())
	}

	// 判断对面是否在线，假如在线则推送过去,假如不在线则结束
	if wsMsg.GroupId > 0 {
		// 获取所有群成员
		chatGroupUser := chat_group_user.AllBy("group_id",helpers.Uint64ToString(wsMsg.GroupId))
		for _,item := range chatGroupUser {
			// 判断群成员是否在线
			groupMemberId := helpers.Uint64ToString(item.BelongId)
			_, ok := wsConn[groupMemberId]
			if !ok {
				fmt.Printf("群成员不在线 %d /n/r",item.BelongId)
			} else {
				// 发送信息的人不发
				if sendId != groupMemberId {
					SendMessage(groupMemberId, messageType, jsonBytes) // 给对面发送
				}
			}
		}
	}else{
		_, ok := wsConn[receiveId]
		if !ok {
			fmt.Printf("好友不在线 %s /n/r",receiveId)
			return
		} else {
			SendMessage(receiveId, messageType, jsonBytes) // 给对面发送
		}
	}
	

}
