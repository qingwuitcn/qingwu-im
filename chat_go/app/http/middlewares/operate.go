package middlewares

import (
	"bytes"
	"io"
	"pro/app/http/locales"
	"pro/app/models/chat_user"
	"pro/pkg/helpers"
	"pro/pkg/jwt"
	"pro/pkg/response"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

// 记录操作信息
func Operate() gin.HandlerFunc {

	return func(c *gin.Context) {
		if len(c.GetHeader("Authorization")) > 0 {
			jwtStruct := jwt.NewJWT()
			JWTCustomClaims, error := jwtStruct.ParserToken(c)
			if error != nil {
				// 判断是否token 已经失效，如果失效则刷新token
				if error.Error() == jwt.ErrTokenExpired.Error() {
					token, err := jwtStruct.RefreshToken(c)
					if err != nil {
						response.Unauthorized(c, locales.T(c,"accountHasExpired"))
						c.Abort()
						return
					} else {
						c.Header("authorization", token)
					}
				} else {
					response.Unauthorized(c, locales.T(c,"accountHasExpired"))
					c.Abort()
					return
				}

			}

			// JWT 解析成功，假如是代理商
			// if JWTCustomClaims.Provider == "Partner" {
			// }

			// 假如是普通用户
			if JWTCustomClaims.Provider == "User" || JWTCustomClaims.Provider == "chatUser" {
				userModel := chat_user.Get(JWTCustomClaims.UserID)
				if userModel.ID == 0 {
					response.Unauthorized(c, locales.T(c,"unableToFindCorrespondingUser"))
					return
				}
				var BelongId uint64 = 0
				if userModel.BelongId == 0 {
					BelongId = userModel.ID
				}
				c.Set("current_user_id", userModel.GetStringID())
				c.Set("current_provider", JWTCustomClaims.Provider)
				c.Set("current_belong_id", cast.ToString(BelongId)) // 获取所属ID 如果为0则是自己
				c.Set("current_user_name", userModel.Username)
				c.Set("current_nickname", userModel.Nickname)
				c.Set("current_user", userModel)
			}

			rowData, _ := io.ReadAll(c.Request.Body)
			if !helpers.Empty(rowData) {
				c.Request.Body = io.NopCloser(bytes.NewBuffer(rowData))
			}

			c.Next()
		} else {
			response.Unauthorized(c, locales.T(c,"accountHasExpired"))
		}

	}
}
