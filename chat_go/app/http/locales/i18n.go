package locales

import (
	"embed"
	"encoding/json"

	ginI18n "github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
	"golang.org/x/text/language"
)

//go:embed *.json
var fs embed.FS
func I18n() gin.HandlerFunc {
	return func(c *gin.Context) {
		lang := c.Request.Header.Get("Accept-Language")
		if c.Request.Header.Get("Custom-Language") != "" {
			lang = c.Request.Header.Get("Custom-Language")
		}
		allLang := []language.Tag{language.MustParse("zh-CN"),language.MustParse("en-US")}
		var matcher = language.NewMatcher(allLang)
		tag, _ :=language.MatchStrings(matcher,lang)
		i18nHandlerFunc := ginI18n.Localize(ginI18n.WithBundle(&ginI18n.BundleCfg{
			DefaultLanguage:  tag,
			FormatBundleFile: "json",
			AcceptLanguage:   allLang,
			RootPath:         "./",
			UnmarshalFunc:    json.Unmarshal,
			// After commenting this line, use defaultLoader
			// it will be loaded from the file
			Loader: &ginI18n.EmbedLoader{
				FS: fs,
			},
		}))
		i18nHandlerFunc(c)
		c.Next()
	}
	
}

func T(c *gin.Context,message string) string{
	return ginI18n.MustGetMessage(c,message)
}