package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type ChatUserFriendRequest struct {
	FriendId     uint64 `valid:"friend_id" json:"friend_id" form:"friend_id"`
	PaginationRequest
}

func ChatUserFriendPagination(data any, c *gin.Context) map[string][]string {
	return Pagination(data, c)
}

func ChatUserFriendSave(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"friend_id":      []string{"required"},
	}
	messages := govalidator.MapData{
		"belong_id": []string{
			"required:好友为必填项",
		},
	}
	return validate(data, rules, messages)
}
