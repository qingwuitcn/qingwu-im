package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type ChatGroupRequest struct {
	Name     string `valid:"name" json:"name" form:"name"`
	Avatar   string `valid:"avatar" json:"avatar" form:"avatar"`
	MemberId string `valid:"member_id" json:"member_id" form:"member_id"`
	PaginationRequest
}

func ChatGroupPagination(data any, c *gin.Context) map[string][]string {
	return Pagination(data, c)
}

func ChatGroupSave(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"name":      []string{"required"},
		"member_id": []string{"required"},
		// "description": []string{"min_cn:3", "max_cn:255"},
	}
	messages := govalidator.MapData{
		"name": []string{
			"required:名称为必填项",
		},
		"member_id": []string{
			"required:群成员为必填项",
		},
	}
	return validate(data, rules, messages)
}
