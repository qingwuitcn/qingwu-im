package requests

import (
    "github.com/gin-gonic/gin"
    "github.com/thedevsaddam/govalidator"
)

type AiRequest struct {
    Prompt        string `valid:"prompt" json:"prompt" form:"prompt"`
    Code        string `valid:"code" json:"code" form:"code"`
    ReceiveId        uint64 `valid:"receive_id" json:"receive_id" form:"receive_id"`
    PaginationRequest
}

func AiPagination(data any,c *gin.Context) map[string][]string {
	return Pagination(data,c)
}

func AiSave(data any, c *gin.Context) map[string][]string {

    rules := govalidator.MapData{
        "receive_id":        []string{"required"},
        "prompt":        []string{"required"},
    }
    messages := govalidator.MapData{
        "receive_id": []string{
            "required:发送对象为必填项",
        },
		"prompt": []string{
            "required:提示词为必填项",
        },
    }
    return validate(data, rules, messages)
}

func SseAiSave(data any, c *gin.Context) map[string][]string {

    rules := govalidator.MapData{
        "code":        []string{"required"},
    }
    messages := govalidator.MapData{
        "code": []string{
            "required:请求码为必填项",
        },
    }
    return validate(data, rules, messages)
}