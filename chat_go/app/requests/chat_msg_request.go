package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type ChatMsgRequest struct {
	GroupId    uint64 `valid:"group_id" json:"group_id" form:"group_id"`
	SendId     uint64 `valid:"send_id" json:"send_id" form:"send_id"`
	ReceiveId  uint64 `valid:"receive_id" json:"receive_id" form:"receive_id"`
	Content    string `valid:"content" json:"content" form:"content"`
	PassType   uint8  `valid:"pass_type" json:"pass_type" form:"pass_type"`
	IsType     uint8  `valid:"is_type" json:"is_type" form:"is_type"`
	PageLastId uint64 `valid:"page_last_id" json:"page_last_id" form:"page_last_id"`
	PaginationRequest
}

func ChatMsgPagination(data any, c *gin.Context) map[string][]string {
	return Pagination(data, c)
}

func ChatMsgSave(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"content": []string{"required"},
	}
	messages := govalidator.MapData{}
	return validate(data, rules, messages)
}

func ChatMsgPassAddFriend(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"pass_type": []string{"required"},
	}
	messages := govalidator.MapData{
		"pass_type": []string{
			"required:是否通过为必填项",
		},
	}
	return validate(data, rules, messages)
}
