package requests

import (
	"mime/multipart"

	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type FileSpaceRequest struct {
	Name        string                `valid:"name" json:"name" form:"name"`
	Filepath    string                `valid:"filepath" json:"filepath" form:"filepath"`
	File        *multipart.FileHeader `valid:"file" json:"file" form:"file"`
	IsDir       uint8                 `valid:"is_dir" json:"is_dir" form:"is_dir"`
	DirId       uint64                `valid:"dir_id" json:"dir_id" form:"dir_id"`
	IsImage     bool                  `valid:"is_image" json:"is_image" form:"is_image"`
	ThumbWidth  int                   `valid:"thumb_width" json:"thumb_width" form:"thumb_width"`
	ThumbHeight int                   `valid:"thumb_height" json:"thumb_height" form:"thumb_height"`

	PaginationRequest
}

func FileSpacePagination(data any,c *gin.Context) map[string][]string {
	return Pagination(data,c)
}

// 图片上传
func FileSpaceSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"name": []string{"required"},
	}
	messages := govalidator.MapData{
		"name": []string{
			"required:名称为必填项",
		},
	}
	return validate(data, rules, messages)
}

// 图片上传
func FileSpaceImageSave(data interface{}, c *gin.Context) map[string][]string {
	
	rules := govalidator.MapData{
		"file:file": []string{"ext:png,jpg,jpeg", "size:20971520", "required"},
		"is_image":  []string{"required"},
	}
	messages := govalidator.MapData{
		"file:file": []string{
			"ext:ext只能上传 png, jpg, jpeg 任意一种的图片",
			"size:文件最大不能超过 20MB",
			"required:必须上传图片",
		},
		"is_image": []string{
			"required:类型必填",
		},
	}
	
	return validateFile(c, data, rules, messages)
}

// 其他文件 上传
func FileSpaceFileSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"file:file": []string{"ext:xls,xlsx,cer,pem,key,crt,rar,zip,bak,apk,doc,docs,txt", "size:20971520", "required"},
		// "is_image":  []string{"required"},
	}
	messages := govalidator.MapData{
		"file:file": []string{
			"ext:ext只能上传指定类型文件如：文档，excel类，zip,rar",
			"size:文件最大不能超过 20MB",
			"required:必须上传文件",
		},
		"is_image": []string{
			"required:类型必填",
		},
	}
	return validateFile(c, data, rules, messages)
}
