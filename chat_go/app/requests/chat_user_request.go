package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type ChatUserRequest struct {
	BelongId    uint64 `valid:"belong_id" json:"belong_id" form:"belong_id"`
	Username    string `valid:"username" json:"username" form:"username"`
	Nickname    string `valid:"nickname" json:"nickname" form:"nickname"`
	Avatar      string `valid:"avatar" json:"avatar" form:"avatar"`
	Phone       string `valid:"phone" json:"phone" form:"phone"`
	Sign       string `valid:"sign" json:"sign" form:"sign"`
	Email       string `valid:"email" json:"email" form:"email"`
	Password   	string `valid:"password" json:"password,omitempty" form:"password"`
	RePassword 	string `valid:"re_password" json:"re_password,omitempty"`
	OldPassword string `valid:"old_password" json:"old_password,omitempty"`
	// Description string `valid:"description" json:"description,omitempty"`
	PaginationRequest
}



func ChatUserPagination(data any, c *gin.Context) map[string][]string {
	return Pagination(data, c)
}

func ChatUserSave(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"username":   []string{"required"},
		"password":   []string{"required"},
		"country_id": []string{"required"},
	}
	messages := govalidator.MapData{
		"username": []string{
			"required:用户名为必填项",
		},
		"password": []string{
			"required:密码为必填项",
		},
		"country_id": []string{
			"required:国家为必填项",
		},
	}
	return validate(data, rules, messages)
}


func ChatUserLogin(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"username": []string{"required"},
		"password": []string{"required"},
		"sign":     []string{"required"},
	}
	messages := govalidator.MapData{
		"username": []string{
			"required:用户名为必填项",
		},
		"password": []string{
			"required:密码为必填项",
		},
		"sign": []string{
			"required:签名为必填项",
		},
	}
	return validate(data, rules, messages)
}

func ChatUserRegister(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"username":   []string{"required"},
		"password":   []string{"required"},
	}
	messages := govalidator.MapData{
		"username": []string{
			"required:用户名为必填项",
		},
		"password": []string{
			"required:密码为必填项",
		},
	}
	return validate(data, rules, messages)
}

func ChatUserForgetPassword(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"username": []string{"required"},
		"password": []string{"required"},
	}
	messages := govalidator.MapData{
		"username": []string{
			"required:用户名为必填项",
		},
		"password": []string{
			"required:密码为必填项",
		},
	}
	return validate(data, rules, messages)
}

func ChatUserEditPassword(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"password": []string{"required"},
		"re_password": []string{"required"},
	}
	messages := govalidator.MapData{
		"password": []string{
			"required:密码为必填项",
		},
		"re_password": []string{
			"required:重复密码为必填项",
		},
	}
	return validate(data, rules, messages)
}
func ChatUserEditPayPassword(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"password": []string{"required"},
	}
	messages := govalidator.MapData{
		"password": []string{
			"required:密码为必填项",
		},
	}
	return validate(data, rules, messages)
}

func ChatUserEditInfo(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"nickname":   []string{"required"},
	}
	messages := govalidator.MapData{
		"nickname": []string{
			"required:用户昵称为必填项",
		},
	}
	return validate(data, rules, messages)
}

func ChatUserSearch(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"username":   []string{"required"},
	}
	messages := govalidator.MapData{
		"username": []string{
			"required:账户为必填项",
		},
	}
	return validate(data, rules, messages)
}
