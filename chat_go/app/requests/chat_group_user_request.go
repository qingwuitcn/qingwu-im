package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type ChatGroupUserRequest struct {
	BelongId     uint64 `valid:"belong_id" json:"belong_id" form:"belong_id"`
	GroupId   uint64 `valid:"group_id" json:"group_id" form:"group_id"`
	PaginationRequest
}

func ChatGroupUserPagination(data any, c *gin.Context) map[string][]string {
	return Pagination(data, c)
}

func ChatGroupUserSave(data any, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"belong_id":      []string{"required"},
		// "description": []string{"min_cn:3", "max_cn:255"},
	}
	messages := govalidator.MapData{
		"belong_id": []string{
			"required:名称为必填项",
		},
	}
	return validate(data, rules, messages)
}
