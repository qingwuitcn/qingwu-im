package requests

import (
    "github.com/gin-gonic/gin"
    "github.com/thedevsaddam/govalidator"
)

type CaptchaRequest struct {
    Username        string `valid:"username" json:"username" form:"username"`
}

func CaptchaSave(data interface{}, c *gin.Context) map[string][]string {

    rules := govalidator.MapData{
        "username":        []string{"required"},
    }
    messages := govalidator.MapData{
        "username": []string{
            "required:名称为必填项",
        },
      
    }
    return validate(data, rules, messages)
}

