// Package routes 注册路由
package routes

import (
	"net/http"

	v1ChatCtr "pro/app/http/controllers/api/v1/chat"
	"pro/app/http/locales"

	"github.com/gin-gonic/gin"
	"pro/app/http/middlewares"
)

// RegisterAPIRoutes 注册网页相关路由
func RegisterAPIRoutes(r *gin.Engine) {
	r.StaticFS("/uploads", http.Dir("./public/uploads"))

	// I18n 是国际化处理中间件
	v1 := r.Group("/v1", locales.I18n())
	{
		// 注册一个路由
		v1.GET("/", func(c *gin.Context) {
			// 以 JSON 格式响应
			c.JSON(http.StatusOK, locales.T(c, "qingwuSystem"))
		})


		captchaCtr := new(v1ChatCtr.CaptchasController)
		wsCtr := new(v1ChatCtr.WsController)
		chatUserCtr := new(v1ChatCtr.ChatUsersController)
		chatUserFriendCtr := new(v1ChatCtr.ChatUserFriendsController)
		chatMsgCtr := new(v1ChatCtr.ChatMsgsController)
		chatGroupCtr := new(v1ChatCtr.ChatGroupsController)
		chatGroupUserCtr := new(v1ChatCtr.ChatGroupUsersController)
		fileSpaceCtr := new(v1ChatCtr.FileSpacesController)
		aiCtr := new(v1ChatCtr.AiController)
		v1.GET("ws", wsCtr.Websocket)
		v1.GET("sse_ai",aiCtr.SseAi)
		

		// 用户登录
		authGroup := v1.Group("auth")
		{
			// user 的注册
			authGroup.POST("captchas", captchaCtr.Store) // 获取滑动验证码目前信息
			authGroup.POST("login", chatUserCtr.Login)
			authGroup.POST("register", chatUserCtr.Register)
			
			
		}
		userGroup := v1.Group("users",middlewares.Operate())
		{
			userGroup.PUT("edit_password", chatUserCtr.EditPassword)
			userGroup.GET("info", chatUserCtr.Show)
			userGroup.PUT("info/:id", chatUserCtr.Update)
			userGroup.GET("get_friends", chatUserCtr.GetFriends)
			userGroup.POST("add_friend", chatUserCtr.AddFriend) // 添加好友
			userGroup.POST("add_group", chatUserCtr.AddGroup) // 申请入群
			userGroup.POST("delete_friend", chatUserFriendCtr.DeleteFriend) // 删除好友
			userGroup.POST("ai",aiCtr.Ai)
		}

		chatMsgGroup := v1.Group("chat_msgs",middlewares.Operate())
		{
			chatMsgGroup.GET("", chatMsgCtr.Index)
			chatMsgGroup.GET("system_logs", chatMsgCtr.SystemLog)
			chatMsgGroup.GET("chating_sigle", chatMsgCtr.ChatingSigle)
			chatMsgGroup.POST("update_unread_friend", chatMsgCtr.UpdateUnreadFriend) // 修改查看状态
			chatMsgGroup.POST("update_unread_group", chatMsgCtr.UpdateUnreadGroup) // 修改群查看状态
			chatMsgGroup.PUT("/:id", chatMsgCtr.Update)
		}
		chatGroupGroup := v1.Group("chat_groups",middlewares.Operate())
		{
			chatGroupGroup.POST("", chatGroupCtr.Store) // 添加群
			chatGroupGroup.GET("/:id", chatGroupCtr.Show) // 查看
			chatGroupGroup.DELETE("/:id", chatGroupCtr.Delete) // 删除
			chatGroupGroup.PUT("/:id", chatGroupCtr.Update) // 编辑
		}
		chatGroupUserGroup := v1.Group("chat_group_users",middlewares.Operate())
		{
			chatGroupUserGroup.DELETE("/:id", chatGroupUserCtr.Delete) // 删除
			chatGroupUserGroup.POST("delete_group_user", chatGroupUserCtr.DeleteUser) // 删除指定用户管理员
		}

		fileSpaceGroup := v1.Group("file_spaces",middlewares.Operate())
		{
			fileSpaceGroup.POST("upload/image",fileSpaceCtr.Image)
			fileSpaceGroup.POST("upload/file",fileSpaceCtr.File)
		}

	
	}
	// v1.Use()
}
