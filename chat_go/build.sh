#!/bin/bash
 
app_name="qingwuit"
platforms=("linux/amd64" "windows/amd64" "darwin/amd64")
 
for platform in "${platforms[@]}" ; do
    platform_split=(${platform//\// })
    GOOS=${platform_split[0]}
    GOARCH=${platform_split[1]}
    output_name=$app_name'-'$GOOS'-'$GOARCH
    if [ $GOOS = "windows" ]; then
        output_name+='.exe'
    fi
 
    echo "Building ${output_name}..."
    env GOOS=$GOOS GOARCH=$GOARCH go build -o $output_name
    if [ $? -ne 0 ]; then
        echo "Failed to build ${output_name}"
        exit 1
    fi
done