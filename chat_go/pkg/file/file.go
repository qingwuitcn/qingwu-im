// Package file 文件操作辅助函数
package file

import (
	"context"
	"crypto/md5"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"pro/app/requests"
	"pro/pkg/app"
	"pro/pkg/auth"
	"pro/pkg/config"
	"pro/pkg/helpers"
	"pro/pkg/imaging"
	"strings"

	"github.com/gin-gonic/gin"
	qiniuAuth "github.com/qiniu/go-sdk/v7/auth"
	qiniuStorage "github.com/qiniu/go-sdk/v7/storage"
	aliOss "github.com/aliyun/aliyun-oss-go-sdk/oss"
)

type FileResource struct {
	Url         string
	Ext         string
	FileName    string
	OldFileName string
	Md5         string
	DiskName    string
	Provider    string
}

// Put 将数据存入文件
func Put(data []byte, to string) error {
	err := os.WriteFile(to, data, 0644)
	if err != nil {
		return err
	}
	return nil
}

// Exists 判断文件是否存在
func Exists(fileToCheck string) bool {
	if _, err := os.Stat(fileToCheck); os.IsNotExist(err) {
		return false
	}
	return true
}

func FileNameWithoutExtension(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}

func SaveUpload(c *gin.Context, request requests.FileSpaceRequest) (FileResource, error) {
	var fileResource FileResource
	// 确保目录存在，不存在创建
	publicPath := "public"
	diskName := config.GetString("STORAGE_DISK", "public")
	provider := auth.CurrentProvider(c)
	dirName := fmt.Sprintf("/uploads/%s/%s/%s/%s/", provider, request.Filepath, app.TimenowInTimezone().Format("2006/01/02"), auth.CurrentUID(c))
	ossTempDir := fmt.Sprintf("/oss_temp/%s/%s/%s/", provider, request.Filepath, app.TimenowInTimezone().Format("2006/01/02"))

	os.MkdirAll(publicPath+dirName, 0755)

	// 保存文件
	fileName := randomNameFromUploadFile(request.File)
	// public/uploads/avatars/2021/12/22/1/nFDacgaWKpWWOmOt.png
	filePath := publicPath + dirName + fileName

	var localSavePath string
	if diskName != "public" {
		localSavePath = publicPath + ossTempDir + fileName
	}else{
		localSavePath = filePath
	}

	// 本地文件
	if err := c.SaveUploadedFile(request.File, localSavePath); err != nil {
		fmt.Println(err)
		return fileResource, err
	}

	fileResource.DiskName = diskName
	fileResource.Url = dirName + fileName
	fileResource.Ext = filepath.Ext(request.File.Filename)
	fileResource.FileName = fileName
	fileResource.OldFileName = request.File.Filename
	fileResource.Provider = provider
	fileResource.Md5 = getMd5(localSavePath)

	if request.IsImage && request.ThumbWidth > 0 && request.ThumbHeight > 0 {
		// 裁切图片
		img, err := imaging.Open(localSavePath, imaging.AutoOrientation(true))
		if err != nil {
			return fileResource, err
		}

		resizeFile := imaging.Thumbnail(img, request.ThumbWidth, request.ThumbHeight, imaging.Lanczos)
		resizeFileName := randomNameFromUploadFile(request.File)
		resizeFilePath := publicPath + dirName + resizeFileName
		fileResource.Md5 = getMd5(resizeFilePath)
		err = imaging.Save(resizeFile, resizeFilePath)
		if err != nil {
			return fileResource, err
		}
		// 删除老文件
		if Exists(filePath) {
			err = os.Remove(filePath)
			if err != nil {
				return fileResource, err
			}
		}
		
		fileResource.Url = dirName + resizeFileName
		localSavePath = resizeFilePath
	}

	err := OssHandle(diskName,localSavePath,filePath,&fileResource)
	if err != nil {
		return fileResource,err
	}

	return fileResource, nil
}

func OssHandle(diskName string,localSavePath string,filePath string,fileResource *FileResource) error{
	// 如果不是OSS
	if diskName != "public" {

		if diskName == "qiniu" {
			qiniuFile,err := qiniuUpload(localSavePath,filePath)
			if err != nil {
				return err
			}
			fileResource.Url = qiniuFile
		}

		if diskName == "alioss" {
			aliFile,err := aliUpload(localSavePath,filePath)
			if err != nil {
				return err
			}
			fileResource.Url = aliFile
		}

		if Exists(localSavePath) {
			err := os.Remove(localSavePath)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func randomNameFromUploadFile(file *multipart.FileHeader) string {
	return helpers.RandomString(16) + filepath.Ext(file.Filename)
}

func getMd5(pathName string) string {
	file, err := os.Open(pathName) // 替换为你的文件路径
	if err != nil {
		return ""
	}
	defer file.Close()
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return ""
	}

	md5Value := hash.Sum(nil)
	md5str := fmt.Sprintf("%x", md5Value)
	return md5str
}

// localFile 本地文件路径  serveFile 保存文件名称
func qiniuUpload(localFile string,serveFile string) (string,error){
	var publicAccessURL string
	bucket := config.GetString("QINIU_BUCKET")
	putPolicy := qiniuStorage.PutPolicy{
		Scope:               bucket,
	}
	mac := qiniuAuth.New(config.GetString("QINIU_KEY"), config.GetString("QINIU_SECRET"))
	upToken := putPolicy.UploadToken(mac)
	cfg := qiniuStorage.Config{}
	// 空间对应的机房
	// cfg.Region = &storage.ZoneHuadong
	// 是否使用https域名
	cfg.UseHTTPS = config.GetBool("QINIU_SSL")
	// 上传是否使用CDN上传加速
	domainUrl := config.GetString("QINIU_DOMAIN")
	if helpers.Empty(domainUrl) {
		cfg.UseCdnDomains = false
	}else{
		cfg.UseCdnDomains =  true
	}
	
	// 构建表单上传的对象
	formUploader := qiniuStorage.NewFormUploader(&cfg)
	ret := qiniuStorage.PutRet{}
	// 可选配置
	putExtra := qiniuStorage.PutExtra{
		// Params: map[string]string{
		// 	"x:name": "github logo",
		// },
	}
	err := formUploader.PutFile(context.Background(), &ret, upToken, serveFile, localFile, &putExtra)
	if err != nil {
		return publicAccessURL,err
	}

	publicAccessURL = qiniuStorage.MakePublicURL(domainUrl, ret.Key)
	return publicAccessURL,err
	// fmt.Println(ret.Key,ret.Hash)
}

// localFile 本地文件路径  serveFile 保存文件名称
func aliUpload(localFile string,serveFile string) (string,error){
	var publicAccessURL string
	bucketName := config.GetString("ALIOSS_BUCKET")
	domainUrl := config.GetString("ALIOSS_CNAME")
	client, err := aliOss.New(config.GetString("ALIOSS_ENDPOINT"),config.GetString("ALIOSS_KEY"), config.GetString("ALIOSS_SECRET"))
    if err != nil {
        return publicAccessURL,err
    }
	bucket, err := client.Bucket(bucketName)
	if err != nil {
        return publicAccessURL,err
    }

	if helpers.Empty(domainUrl) {
		domainUrl = config.GetString("ALIOSS_ENDPOINT")
		if config.GetBool("ALIOSS_SSL") {
			domainUrl = "https://" + domainUrl
		}else{
			domainUrl = "http://" + domainUrl
		}
	}else{
		_,err := client.CreateBucketCnameToken(bucketName,domainUrl)
		if err != nil {
			return publicAccessURL,err
		}
	}
	
	// 上传文件。
    err = bucket.PutObjectFromFile(serveFile, localFile)
    if err != nil {
        return publicAccessURL,err
    }

	publicAccessURL = domainUrl + "/" +serveFile
	return publicAccessURL,err
	// fmt.Println(ret.Key,ret.Hash)
}

