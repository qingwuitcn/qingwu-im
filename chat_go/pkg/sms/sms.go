package sms

import (
	"errors"
	"pro/pkg/config"
	"github.com/alibabacloud-go/darabonba-openapi/client"
	dysmsapi20170525 "github.com/alibabacloud-go/dysmsapi-20170525/v2/client"
)

type SmsData struct{
	Phone string
	Platform string // 平台默认阿里云
	Template string // 模板
	SignName string // 签名
	Content string // 内容
	Code string // 内容
	
}

// 临时使用阿里云暂时没其他平台测试
func SendSms(smsData SmsData) (smsResults *dysmsapi20170525.SendSmsResponse, err error){
	AccessKeyId := config.GetString("AliSMS_KEY")
	AccessKeySecret := config.GetString("AliSMS_SECRET")
	endpoint := "dysmsapi.aliyuncs.com"
	c := &client.Config{AccessKeyId: &AccessKeyId, AccessKeySecret: &AccessKeySecret, Endpoint: &endpoint}
	newClient, err := dysmsapi20170525.NewClient(c)
	if err != nil {
		return smsResults,err
	}

	code := "{\"code\":" + smsData.Code + "}"

	request := &dysmsapi20170525.SendSmsRequest{
		PhoneNumbers:  &smsData.Phone,
		TemplateCode:  &smsData.Template,
		SignName:      &smsData.SignName,
		TemplateParam: &code,
	}

	sms, err := newClient.SendSms(request)
	if err != nil {
		return sms,err
	}

	if *sms.StatusCode != 200 || *sms.Body.Code != "OK" {
		return sms,errors.New(*sms.Body.Message)
	}
	return sms,nil
	
}