package auth

import (
	"errors"
	"pro/app/models/chat_user"
	"pro/pkg/logger"

	"github.com/gin-gonic/gin"
)

// CurrentUser 从 gin.context 中获取当前登录用户
func CurrentAdminUser(c *gin.Context) chat_user.ChatUser {
    userModel, ok := c.MustGet("current_user").(chat_user.ChatUser)
    if !ok {
        logger.LogIf(errors.New("无法获取用户"))
        return chat_user.ChatUser{}
    }
    return userModel
}

// CurrentUID 从 gin.context 中获取当前登录用户 ID
func CurrentUID(c *gin.Context) string {
    return c.GetString("current_user_id")
}

// CurrentProvider 从 gin.context 中获取当前登录用户 provider
func CurrentProvider(c *gin.Context) string {
    return c.GetString("current_provider")
}

// CurrentBelongId 从 gin.context 中获取当前登录用户 belong_id
func CurrentBelongId(c *gin.Context) string {
    return c.GetString("current_belong_id")
}

// CurrentBelongId 从 gin.context 中获取当前登录用户 belong_id
func CurrentIsSuper(c *gin.Context) string {
    return c.GetString("current_is_super")
}