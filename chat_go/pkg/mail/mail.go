// Package mail 发送短信 我认为不需要单例修改了
package mail

import (
	"fmt"
	"net/smtp"
	"pro/pkg/config"
	"pro/pkg/logger"

	mailPKG "github.com/jordan-wright/email"
)

// 发送邮件
func SendEmail(email ,content string) (err error) {
	newClient := mailPKG.NewEmail()
	newClient.From = fmt.Sprintf("%v <%v>", config.GetString("EMAIL_FROM_NAME"), config.GetString("EMAIL_FROM_ADDRESS"))
	newClient.To = []string{email}
	// newClient.Bcc = []string{"test_bcc@example.com"} // 抄送
	// newClient.Cc = []string{"test_cc@example.com"} // 暗抄送 暂无需求
	newClient.Subject = config.GetString("EMAIL_SUBJECT")
	// newClient.Text = []byte("Text Body is, of course, supported!")
	newClient.HTML = []byte(content)
	err = newClient.Send(fmt.Sprintf("%v:%v", config.GetString("EMAIL_HOST"), config.GetString("EMAIL_PORT")), smtp.PlainAuth("", config.GetString("EMAIL_USERNAME"), config.GetString("EMAIL_PASSWORD", ""), config.GetString("EMAIL_HOST")))

	if err != nil {
		logger.DebugJSON("Email", "发件详情", newClient)
		logger.ErrorString("Email", "发件出错", err.Error())
		return err
	}

	return nil

}
