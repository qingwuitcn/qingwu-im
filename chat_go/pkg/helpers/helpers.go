// Package helpers 存放辅助方法
package helpers

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	mathrand "math/rand"
	"net"
	"net/http"
	"net/url"
	"os"
	"pro/app/models"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

// Empty 类似于 PHP 的 empty() 函数
// Empty 类似于 PHP 的 empty() 函数
func Empty(val interface{}) bool {
	if val == nil {
		return true
	}
	v := reflect.ValueOf(val)
	switch v.Kind() {
	case reflect.String, reflect.Array:
		return v.Len() == 0
	case reflect.Map, reflect.Slice:
		return v.Len() == 0 || v.IsNil()
	case reflect.Bool:
		return !v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return v.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return v.Float() == 0
	case reflect.Interface, reflect.Ptr:
		return v.IsNil()
	}
	return reflect.DeepEqual(val, reflect.Zero(v.Type()).Interface())
}

func IsNum(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

// 转化int
func ToNum(s string) uint64 {
	num, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		num = 0
	}
	return num
}

func Uint64ToString(num uint64) string{
	return strconv.FormatUint(num,10)
}

func InSliceString(slice []string, target string) bool {
    for _, num := range slice {
        if num == target {
            return true
        }
    }
    return false
}

// uint64 转化
func SliceToString(slice []uint64) string{
	strSlice := make([]string, len(slice))
	for i, num := range slice {
        strSlice[i] = Uint64ToString(num)
    }
	return strings.Join(strSlice, ",")
}

// 字符串格式时间转化为time.time
func StrToJsonTime(str string) models.JsonTime {
	time, _ := time.Parse("2006-01-02 15:04:05", str)
	return models.JsonTime(time)  
}

// MicrosecondsStr 将 time.Duration 类型（nano seconds 为单位）
// 输出为小数点后 3 位的 ms （microsecond 毫秒，千分之一秒）
func MicrosecondsStr(elapsed time.Duration) string {
	return fmt.Sprintf("%.3fms", float64(elapsed.Nanoseconds())/1e6)
}

// FirstElement 安全地获取 args[0]，避免 panic: runtime error: index out of range
func FirstElement(args []string) string {
	if len(args) > 0 {
		return args[0]
	}
	return ""
}

// RandomString 生成长度为 length 的随机字符串
func RandomString(length int) string {
	mathrand.New(mathrand.NewSource(time.Now().UnixNano()))
	letters := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, length)
	for i := range b {
		b[i] = letters[mathrand.Intn(len(letters))]
	}
	return string(b)
}

// 指定0 - 区域随机数字
func RandomNum(start int, end int) string {
	r := mathrand.New(mathrand.NewSource(time.Now().UnixNano()))
	randNum := r.Intn(end)
	var str string
	if randNum >= start {
		str = strconv.Itoa(randNum)
	} else {
		nums := randNum + start
		str = strconv.Itoa(nums)
	}
	return str
}

// 生成Md5
func MakeMd5(str string) string {
	h := md5.New()
	io.WriteString(h, str)
	return hex.EncodeToString(h.Sum(nil))
}

// htpp请求
func HttpGet(url string, params map[string]string) ([]byte, error) {
	resp, err := http.Get(ConvertToQueryParams(url, params))
	return ResponseHandle(resp, err)
}

func HttpPost(url string, params map[string]string, body string) ([]byte, error) {
	resp, err := http.Post(ConvertToQueryParams(url, params), "application/json", strings.NewReader(body))
	return ResponseHandle(resp, err)
}

func ConvertToQueryParams(requrl string, params map[string]string) string {
	data := url.Values{}
	for key, value := range params {
		data.Set(key, value)
	}
	u, _ := url.ParseRequestURI(requrl)
	u.RawQuery = data.Encode()
	return u.String()
}

// 也可以返回成string，改写最后的return为string(b)
func ResponseHandle(resp *http.Response, err error) ([]byte, error) {
	if err != nil {
		return []byte(""), err
	}
	b, err := io.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return []byte(""), err
	}
	return b, nil
}

// 读取文件内容返回 字节切片
func ReadFileContent(filename string) ([]byte,error) {
	content, err := os.ReadFile(filename)
    if err != nil {
        return content, err
    }
    return content, nil
}

// json解码 interface{}
func JsonDecode(data []byte, Response struct{}) struct{} {
	json.Unmarshal(data, &Response)
	return Response
}

// json编码 interface{}
func JsonEncode(Response struct{}) []byte {
	jsonBytes, err := json.Marshal(Response)
	if err != nil {
		fmt.Println(err)
	}
	return jsonBytes
}

// 获取真实IP
func GetRealIp(c *gin.Context) string {
	// 获取IP
	ip := c.Request.Header.Get("X-Real-IP")
	if ip == "" {
		ip = c.Request.Header.Get("X-Forwarded-For")
	}
	if ip == "" {
		ip = c.Request.RemoteAddr
	}
	return ip
}

// 获取MAC地址
func GetMac() (macAddrs []string) {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		fmt.Printf("fail to get net interfaces: %v\n", err)
		return macAddrs
	}

	for _, netInterface := range netInterfaces {
		macAddr := netInterface.HardwareAddr.String()
		if len(macAddr) == 0 {
			continue
		}
		macAddrs = append(macAddrs, macAddr)
	}
	return macAddrs
}

// 获取本机IP
func GetLocalIps() (ips []string) {
	interfaceAddr, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Printf("fail to get net interfaces ipAddress: %v\n", err)
		return ips
	}

	for _, address := range interfaceAddr {
		ipNet, isVailIpNet := address.(*net.IPNet)
		// 检查ip地址判断是否回环地址
		if isVailIpNet && !ipNet.IP.IsLoopback() {
			if ipNet.IP.To4() != nil {
				ips = append(ips, ipNet.IP.String())
			}
		}
	}
	return ips
}
