package bootstrap

import (
	"errors"
	"fmt"
	"pro/pkg/config"
	"pro/pkg/database"
	"pro/pkg/logger"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	// "gorm.io/gorm/logger"
)

// SetupDB 初始化数据库和 ORM
func SetupDB() {

	var dbConfig gorm.Dialector
	switch config.Get("database.connection") {
	case "mysql":
		// 构建 DSN 信息
		dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=%v&parseTime=True&multiStatements=true&loc=Local",
			config.Get("database.mysql.username"),
			config.Get("database.mysql.password"),
			config.Get("database.mysql.host"),
			config.Get("database.mysql.port"),
			config.Get("database.mysql.database"),
			config.Get("database.mysql.charset"),
		)
		dbConfig = mysql.New(mysql.Config{
			DSN: dsn,
		})
	case "sqlite":
		// 初始化 sqlite
		// database := config.Get("database.sqlite.database")
		// dbConfig = sqlite.Open(database)
	default:
		panic(errors.New("database connection not supported"))
	}

	// 连接数据库，并设置 GORM 的日志模式
	// database.Connect(dbConfig, logger.Default.LogMode(logger.Info))
	database.Connect(dbConfig, logger.NewGormLogger())

	// 判断是否开启关联数据库
	if config.GetBool("DB_A_OPEN") && !config.GetBool("DB_A_SAME"){
		database.AssociationConnect(dbConfig, logger.NewGormLogger())
		// 设置最大连接数
		database.AssociationSQLDB.SetMaxOpenConns(config.GetInt("database.AssociationMysql.max_open_connections"))
		// 设置最大空闲连接数
		database.AssociationSQLDB.SetMaxIdleConns(config.GetInt("database.AssociationMysql.max_idle_connections"))
		// 设置每个链接的过期时间
		database.AssociationSQLDB.SetConnMaxLifetime(time.Duration(config.GetInt("database.AssociationMysql.max_life_seconds")) * time.Second)
	}
	

	// 设置最大连接数
	database.SQLDB.SetMaxOpenConns(config.GetInt("database.mysql.max_open_connections"))
	// 设置最大空闲连接数
	database.SQLDB.SetMaxIdleConns(config.GetInt("database.mysql.max_idle_connections"))
	// 设置每个链接的过期时间
	database.SQLDB.SetConnMaxLifetime(time.Duration(config.GetInt("database.mysql.max_life_seconds")) * time.Second)
}
