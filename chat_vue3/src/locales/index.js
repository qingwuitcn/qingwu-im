import enUs from "./en-US.json";
import zhCn from "./zh-CN.json";
import zhTw from "./zh-TW.json";
import ruRU from "./ru-RU.json";
import jaJP from "./ja-JP.json";
import deDE from "./de-DE.json";
import es from "./es.json";

export default {
  "zh-CN": zhCn,
  "zh-SG": zhCn,
  "zh-HK": zhTw,
  "zh-TW": zhTw,
  "en-US": enUs,
  "ru-RU": ruRU,
  "ja-JP": jaJP,
  "de-DE": deDE,
  es: es,
};
