import { ref, computed, reactive } from "vue";
import { defineStore } from "pinia";
import { ElMessage, useId } from "element-plus";

export const useWsStore = defineStore("ws", () => {
  const ws = ref(null); // ws 连接对象
  const connState = ref(false); // 连接状态
  let hostname = window.location.hostname;
  if (window.location.port && window.location.port !== "80") {
    hostname += ":" + window.location.port;
  }
  // "ws://127.0.0.1:3006/v1/ws"  "ws://" + window.location.hostname + "/api/ws"
  const wsOption = reactive({
    url: "ws://" + hostname + "/api/ws", // ws连接地址
    reconnectionTime: 5, // 重新连接时间s秒
  });
  let messageParams = {
    token: localStorage.getItem("token"),
    group_id: 0,
    send_id: 0,
    receive_id: 0,
    code: "receive",
    is_type: 1,
    content: "",
  };
  let timer = null; // ws 断开重连定时任务
  const messageRev = ref({});
  const messageApiRev = reactive({
    chating: [],
    groups: [],
    friends: [],
    curChat: {},
    curChatMsg: [],
    chatingHeight: 0,
    curChatMsgTotal: 0, // 聊天条数总页数
    curChatMsgLoading: false,
    messageType: [
      "",
      "[图片]",
      "[文件]",
      "[分享]",
      "[语音]",
      "[系统消息]",
      "[通知消息]",
    ],
  });
  const chatListParams = ref({
    page: 1,
    page_last_id: 0,
    per_page: 10,
  });

  // 初始化ws
  const initWebsocket = () => {
    if (ws.value != null) return console.log("socket runing"); // socket 存在
    ws.value = new WebSocket(wsOption.url);

    ws.value.onopen = onOpen;
    ws.value.onclose = onClose;
    ws.value.onerror = onError;
    ws.value.onmessage = onMessage;

    // 检测是否断开连接
    checkConnect();
  };

  const checkConnect = () => {
    if (timer != null) return;
    timer = setInterval(() => {
      if (ws.value != null && ws.value.readyState != 1) {
        initWebsocket();
      }
    }, wsOption.reconnectionTime * 5000);
  };

  // 打开
  const onOpen = (e) => {
    console.log("✨ %c青梧IM：连接服务端成功!", `color:#337ecc;`);
    connState.value = ws.value.readyState == 1 ? true : false;

    // 绑定用户ID
    bindUser();
    // 默认需要获取聊天列表
    sendMessage({ code: "get_chat_lists" });
  };

  // 关闭
  const onClose = (e) => {
    if (ws.value != null) ws.value.close();
    ws.value = null;
    connState.value = false;
    console.log("onClose", e); // 服务端断开或者有一方断开导致关闭
  };

  // 错误
  const onError = (e) => {
    connState.value = false;
    console.log("onError", e);
  };

  // 信息接收
  const onMessage = async (e) => {
    // 解压数据
    try {
      let resp = JSON.parse(e.data);
      if (resp.code == "get_chat_lists") {
        if (resp.content != null) {
          messageApiRev.chating = JSON.parse(resp.content);
        }
      } else if (resp.code == "get_friends") {
        if (resp.content != null) {
          messageApiRev.friends = JSON.parse(resp.content);
        }
      } else if (resp.code == "get_groups") {
        if (resp.content != null) {
          messageApiRev.groups = JSON.parse(resp.content);
        }
      } else if (resp.code == "auth_error") {
        window.location.href = "/login";
      } else {
        messageRev.value = resp;

        // 假如是被群解散消息 drop_group || 被群主踢出群 user_leave_group || 好友删除  friend_leave
        if (
          [
            "user_leave_group",
            "leader_delete",
            "drop_group",
            "friend_leave",
          ].includes(resp.code)
        ) {
          if (
            messageApiRev.chating.length &&
            messageApiRev.chating.length > 0
          ) {
            // console.log(resp, "===============");
            let delIndex = -1;
            let partner_id = -1;
            if (resp.code == "leader_delete" || resp.code == "drop_group") {
              partner_id = resp.group_id;
            } else {
              partner_id = resp.content;
            }
            messageApiRev.chating.map((item, index) => {
              if (
                item.group_id == resp.group_id &&
                item.chat_partner_id == partner_id
              ) {
                delIndex = index;
              }
            });
            if (delIndex > -1) {
              messageApiRev.chating.splice(delIndex, 1);
            }
            // 如果正在当前页面则直接 设置curChat.disabled = true
            if (isCurChatPage(resp)) {
              messageApiRev.curChat.disabled = true;
            }
          }
          // 如果是好友就加载好友，不是就群
          if (resp.code == "friend_leave") {
            sendMessage({ code: "get_friends" });
          } else {
            sendMessage({ code: "get_groups" });
          }
        }

        // 假如接收到信息
        if (resp.code == "") {
          // 获取好友信息插入本地
          let rs = await R.get("chat_msgs/chating_sigle", {
            send_id: resp.send_id,
            receive_id: resp.receive_id,
            is_type: resp.is_type,
            group_id: resp.group_id,
            code: resp.code,
          });

          if (rs.code && rs.code == 200) {
            // 如果正在聊天界面则开始插入
            if (isCurChatPage(resp)) {
              rs.data.unread = 0; // 未读状态不显示
              let msg = {
                chat_user: {
                  id:
                    rs.data.receive_id == 0 ? rs.data.send_id : rs.data.send_id,
                  nickname:
                    rs.data.receive_id == 0
                      ? rs.data.send_nickname
                      : rs.data.send_nickname,
                  avatar:
                    rs.data.receive_id == 0
                      ? rs.data.send_avatar
                      : rs.data.send_avatar,
                },
                msg_id: rs.data.msg_id,
                chat_partner_id: resp.send_id,
                group_id: resp.group_id,
                is_type: resp.is_type,
                send_id: resp.send_id,
                receive_id: resp.receive_id,
                content:
                  resp.is_type == 1
                    ? rs.data.content
                    : JSON.parse(rs.data.content),
                show_diff_time: rs.data.show_diff_time,
              };
              if (resp.is_type > 1) {
                rs.data.content = messageApiRev.messageType[resp.is_type];
              }
              addCurChatMsg(msg);
              // 清除两人未读信息状态 || 群聊天未读信息情况
              if (messageApiRev.curChat.group_id > 0) {
                await R.post("chat_msgs/update_unread_group", {
                  group_id: messageApiRev.curChat.group_id,
                });
              } else {
                await R.post("chat_msgs/update_unread_friend", {
                  send_id: resp.send_id,
                });
              }
            }
            setChatingSort(rs.data); // 更新排序
          }
        }

        // 查询到成为好友的用户信息，插入自己的列表
        // 暂时先直接获取所有，可以根据情况调取sigle方法
        // sendMessage({ code: "get_chat_lists" });
      }
    } catch (e) {
      console.log(e);
    }
  };

  // 信息发送
  const sendMessage = async (e) => {
    if (ws.value == null) return ElMessage.error("未连接网络无法发送");
    if (ws.value.readyState == 0)
      return ElMessage.error("未连接网络无法发送 - 2");
    if (ws.value.readyState == 2) return ElMessage.error("与服务端连接关闭中");
    if (ws.value.readyState == 3) return ElMessage.error("与服务端断开连接");
    let sendParams = JSON.parse(JSON.stringify(messageParams));
    let params = Object.assign(sendParams, e);
    ws.value.send(JSON.stringify(params));
  };

  // 绑定用户
  const bindUser = () => {
    sendMessage({ code: "bind_user" });
  };

  // 判断是否是当前页面
  const isCurChatPage = (resp) => {
    return (
      (messageApiRev.curChat.send_id == resp.send_id &&
        messageApiRev.curChat.receive_id == resp.receive_id) ||
      (messageApiRev.curChat.send_id == resp.receive_id &&
        messageApiRev.curChat.receive_id == resp.send_id) ||
      (messageApiRev.curChat.group_id != 0 &&
        messageApiRev.curChat.group_id == resp.group_id &&
        messageApiRev.curChat.chat_partner_id == resp.group_id)
    );
  };

  // 设置当前聊天用户
  const setCurChat = async (e) => {
    messageApiRev.curChat = e;
  };

  // 删除指定聊天列表
  const delChating = async (e) => {
    let index = -1;
    if (messageApiRev.chating.length && messageApiRev.chating.length > 0) {
      messageApiRev.chating.map((item, key) => {
        if (
          item.chat_partner_id == e.chat_partner_id &&
          item.group_id == e.group_id &&
          e.chat_partner_id != 0
        ) {
          index = key;
        }
      });
      if (index > -1) {
        messageApiRev.chating.splice(index, 1);
      }
    }
  };
  // 跳转新聊天框
  const jumpNewCurChat = () => {
    if (messageApiRev.chating.length && messageApiRev.chating.length > 0) {
      if (messageApiRev.chating[0].chat_partner_id != 0) {
        setCurChat(messageApiRev.chating[0]);
      } else {
        H.isEmpty(messageApiRev.chating[1])
          ? window.reload()
          : setCurChat(messageApiRev.chating.value[0]);
      }
    } else {
      window.reload();
    }
  };

  // 查询聊天记录
  const loadCurMsg = async () => {
    messageApiRev.curChatMsgLoading = true;
    let rs = await R.get("chat_msgs", chatListParams.value);
    if (rs.code && rs.code == 200) {
      // 第一页直接set
      if (chatListParams.value.page == 1) {
        setCurChatMsg([]);
        if (rs.data && rs.data.length && rs.data.length > 0) {
          if (rs.data != null && rs.data.length && rs.data.length > 0) {
            chatListParams.value.page_last_id = rs.data[0].id;
          }
          setCurChatMsg(rs.data);
        }
      } else {
        if (rs.data && rs.data.length && rs.data.length > 0) {
          addPageCurChatMsg(rs.data);
        }
      }
      messageApiRev.curChatMsgTotal = rs.pager.TotalPage || 1;
    }
    messageApiRev.curChatMsgLoading = false;
  };
  // 设置查询的参数
  const setChatListParams = (e) => {
    chatListParams.value = Object.assign(chatListParams.value, e);
  };
  // 设置聊天记录
  const setCurChatMsg = async (e) => {
    messageApiRev.curChatMsg = e.reverse();
  };
  // 上滚增加聊天记录
  const addPageCurChatMsg = async (e) => {
    messageApiRev.curChatMsg.unshift(...e.reverse());
  };
  // 插入新聊天记录
  const addCurChatMsg = async (e) => {
    messageApiRev.curChatMsg.push(e);
  };

  // 判断是否存在并将数据置顶
  const setChatingSort = (e, sortType) => {
    sortType = sortType || false;
    if (
      messageApiRev.chating != null &&
      messageApiRev.chating.length &&
      messageApiRev.chating.length > 0
    ) {
      let index = -1;
      for (let i = 0; i < messageApiRev.chating.length; i++) {
        if (
          (messageApiRev.chating[i].chat_partner_id == e.chat_partner_id &&
            messageApiRev.chating[i].group_id == e.group_id) ||
          (messageApiRev.chating[i].chat_partner_id == 0 &&
            messageApiRev.chating[i].code != "")
        ) {
          index = i;
        }
      }
      // 如果允许排序
      if (!sortType) {
        if (index >= 0) messageApiRev.chating.splice(index, 1);
        messageApiRev.chating.unshift(e);
      } else {
        messageApiRev.chating[index] = e;
      }
    } else {
      messageApiRev.chating = [e];
    }
  };

  // 设置聊天框高度
  const setChatingHeight = (e) => (messageApiRev.chatingHeight = e);

  // 设置未读状态
  const setUnread = async (e) => {
    if (messageApiRev.chating[e]) messageApiRev.chating[e].unread = 0;
  };

  return {
    ws,
    connState,
    messageRev,
    messageApiRev,
    chatListParams,
    sendMessage,
    initWebsocket,
    bindUser,
    setCurChat,
    setUnread,
    onMessage,
    loadCurMsg,
    setChatListParams,
    setCurChatMsg,
    addPageCurChatMsg,
    addCurChatMsg,
    setCurChat,
    setChatingSort,
    setChatingHeight,
    delChating,
    jumpNewCurChat,
  };
});

// chatMsg := chat_msg.ChatMsg{
//     GroupId: wsMsg.GroupId,
//     SendId: wsMsg.SendId,
//     ReceiveId: wsMsg.ReceiveId,
//     Code: "receive", // 那边接收消息
//     IsType: wsMsg.IsType,
//     Content: wsMsg.Content,
//     ShowDiffTime: ShowDiffTime,
// }

//
// 仅当所有现有的数据都已被发送出去时，再发送更多数据
// setInterval(() => {
//     if (socket.bufferedAmount == 0) {
//       socket.send(moreData());
//     }
//   }, 100);
// 连接状态
// 要获取连接状态，可以通过带有值的 socket.readyState 属性：

// 0 —— “CONNECTING”：连接还未建立，
// 1 —— “OPEN”：通信中，
// 2 —— “CLOSING”：连接关闭中，
// 3 —— “CLOSED”：连接已关闭。
