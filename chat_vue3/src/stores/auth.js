import { defineStore } from "pinia";
import router from "@/router";
import { H } from "@/plugins/helper";
import { R } from "@/plugins/http";
export const useAuthStore = defineStore("auth", {
  state: () => ({
    mode: "chatUser",
    user: {
      id: "",
      username: "-",
      nickname: "-",
      avatar: undefined,
    },
    countryId: 0,
    cart_num: 0,
    country: null,
    isLogin: false,
  }),
  getters: {
    // double: (state) => state.count * 2,
  },
  actions: {
    // increment() {
    //     this.count++
    // },
    async setUser(user) {
      this.user.username = user.username || "-";
      this.user.nickname = user.nickname || "-";
      if (!H.isEmpty(user.id)) this.user.id = user.id;
      if (!H.isEmpty(user.money)) this.user.money = user.money;
      if (!H.isEmpty(user.avatar)) this.user.avatar = user.avatar;
      if (!H.isEmpty(user.phone)) this.user.phone = user.phone;
      if (!H.isEmpty(user.email)) this.user.email = user.email;
      if (!H.isEmpty(user.token)) localStorage.setItem("token", user.token);
      this.isLogin = true;
    },

    // 根据token获取WEB用户信息
    async loadUserInfo() {
      await this.loadLoginStatus();
      if (!this.isLogin) return;
      let rs = await R.get("users/info");
      if (rs && rs.code == 200) {
        await this.setUser(rs.data);
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
    },

    async loadUserInfo2() {
      await this.loadLoginStatus();
      let rs = await R.get("users/info");
      if (rs && rs.code == 200) {
        await this.setUser(rs.data);
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
    },

    // 总后台密码修改
    async eidtPassword(user) {
      let res = await R.put("users/edit_password", user);
      if (res && res.code == 200) return true;
      return false;
    },

    logout() {
      this.user.roles = [];
      this.user.username = "-";
      this.user.nickname = "-";
      try {
        localStorage.removeItem("token");
      } catch (err) {
        console.log(err);
      }
      localStorage.removeItem("token");
      this.isLogin = false;
      return router.push("/login");
    },
    setMode(e) {
      this.mode = e;
    },
    async loadLoginStatus() {
      let token = "";
      try {
        token = localStorage.getItem("token");
      } catch (error) {
        console.log(error);
        return (this.isLogin = false);
      }
      if (!H.isEmpty(token)) return (this.isLogin = true);
      return (this.isLogin = false);
    },
  },
});
