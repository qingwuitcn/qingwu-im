import { ref } from "vue";
import { defineStore } from "pinia";

export const useMenuStore = defineStore("menu", () => {
  const tabIndex = ref(1);
  const setTabIndex = (e) => {
    tabIndex.value = e;
  };

  return { tabIndex, setTabIndex };
});
