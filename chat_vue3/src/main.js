import "./assets/base.scss";

import { createApp } from "vue";
import { createPinia } from "pinia";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "element-plus/theme-chalk/dark/css-vars.css";

import App from "./App.vue";
import router from "./router";
import { loadLangElementPlus, getLang } from "./plugins/config";
import i18n from "./plugins/i18n";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";

library.add(fas, far);

// 语种
import zhCn from "element-plus/es/locale/lang/zh-cn";
import zhTw from "element-plus/es/locale/lang/zh-tw";
import en from "element-plus/es/locale/lang/en"; // en
import de from "element-plus/es/locale/lang/de"; // 德
import ja from "element-plus/es/locale/lang/ja"; // 日
import es from "element-plus/es/locale/lang/es"; // 西班牙
import pt from "element-plus/es/locale/lang/pt"; // 葡萄牙
import ptBr from "element-plus/es/locale/lang/pt-br"; // 葡萄牙巴西
import th from "element-plus/es/locale/lang/th"; // 泰语
import vi from "element-plus/es/locale/lang/vi"; // 越南
import ar from "element-plus/es/locale/lang/ar"; // 阿拉伯
import ru from "element-plus/es/locale/lang/ru"; // 俄语

let ElementPlusLang = loadLangElementPlus(getLang(), {
  zhCn,
  zhTw,
  en,
  de,
  ja,
  es,
  pt,
  ptBr,
  th,
  vi,
  ar,
  ru,
});
console.log(ElementPlusLang);
const app = createApp(App);

app
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(createPinia())
  .use(router)
  .use(i18n)
  .use(ElementPlus, {
    size: "default",
    zIndex: 3000,
    locale: ElementPlusLang,
  })
  .mount("#app");
