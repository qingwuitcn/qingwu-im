import axios from "axios";
import { isEmpty } from "lodash";
import { ElMessage } from "element-plus";
import { REG_PROHIBIT, makeSignature } from "@/plugins/config";
import router from "@/router";

axios.defaults.timeout = 30000; // 请求超时
axios.defaults.headers.post["Content-Type"] = "application/json;";
axios.defaults.headers.put["Content-Type"] = "application/json;";
// axios.defaults.withCredentials = true; // 允许跨域携带cookie

const prefixUri = "/api/";

// 添加请求拦截器
axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // 获取localStorage 内token
    let token = "";
    try {
      token = localStorage.getItem("token");
    } catch (error) {
      console.log(error);
    }
    if (!isEmpty(token)) {
      config.headers["Authorization"] = "Bearer " + token; // 如果token 存在则携带token访问
    }

    // 设置语种
    let lang = localStorage.getItem("qw_language");
    if (lang != "" && lang != undefined) {
      config.headers["Custom-Language"] = lang;
    }

    if (config.method == "get") {
      config.headers["Signature"] = makeSignature(config.params || {});
    } else {
      config.headers["Signature"] = makeSignature(config.data || {});
    }

    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (res) {
    // 对响应数据做点什么
    // if(res.status != 200){
    //     ElMessage.error(res.statusText);
    // }
    // 如果出现401 代表token 失效
    if (res.data.code == 401) {
      ElMessage.error(res.data.msg);
      // 下面写跳转 删除token
      localStorage.removeItem("token");
      window.location = "/login";
      return;
    }

    // 429 代表请求太频繁
    if (res.data.code == 429) {
      return ElMessage.error("您请求太频繁了，请休息一会");
    }

    // 刷新了token 则重新存放
    if (!isEmpty(res.headers.authorization)) {
      var token = res.headers.authorization.split(" ")[1];
      localStorage.setItem("token", token);
    }

    if (res.data) return res.data;
    // 防止多次出现
    // ElMessage.destroy();
    return res;
  },
  function (err) {
    // 如果地址无法请求
    if (isEmpty(err.response)) {
      console.error("网络或接口异常，请检查！");
      console.info(err);
    }

    // 存在状态码
    if (err.response.status) {
      if (err.response.data == "") {
        ElMessage.error(err.response.statusText);
      } else {
        switch (err.response.status) {
          case 404:
            ElMessage.error(err.response.data.msg);
            break;
          case 500:
            ElMessage.error(err.response.data.msg);
            break;
          case 401:
            // token 失效 这里可以加删除token 信息
            ElMessage.error(err.response.data.msg);
            // 下面写跳转 删除token
            localStorage.removeItem("token");
            window.location = "/login";
            break;
          default:
            ElMessage.error(err.response.data.msg);
            break;
        }
      }
    } else {
      ElMessage.error(err.response.statusText);
    }

    // 对响应错误做点什么
    return Promise.reject(err.response.data);
  }
);

export const R = {
  /**
   * get方法，对应get请求
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   * @param {Boolean} isSource [是否自定义连接]
   */
  get: (url, params, isSource = false) => {
    return new Promise((resolve, reject) => {
      axios
        .get((!isSource ? prefixUri : "") + url, {
          params: params,
        })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err.data);
        });
    }).catch(() => {});
  },

  put: (url, params, isSource = false) => {
    return new Promise((resolve, reject) => {
      axios
        .put((!isSource ? prefixUri : "") + url, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err.data);
        });
    }).catch(() => {});
  },

  deletes: (url, params, isSource = false) => {
    return new Promise((resolve, reject) => {
      axios
        .delete((!isSource ? prefixUri : "") + url, {
          params: params,
        })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err.data);
        });
    }).catch(() => {});
  },
  /**
   * post方法，对应post请求
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   * @param {Boolean} isSource [是否自定义连接]
   */
  post: (url, params, isSource = false) => {
    return new Promise((resolve, reject) => {
      axios
        .post((!isSource ? prefixUri : "") + url, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err.data);
        });
    }).catch(() => {});
  },

  /**
   * post方法，对应post请求,file 文件上传
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   */
  postfile: (url, params, isSource = false) => {
    return new Promise((resolve, reject) => {
      axios
        .post((!isSource ? prefixUri : "") + url, params, {
          headers: { "Content-Type": "multipart/form-data" },
        })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err.data);
        });
    });
  },

  /**
   * vue 路由跳转
   * @param {String} url [跳转的url地址]
   * @param {Object} type [跳转的类型]
   */
  nav: (url, is_type) => {
    let uriType = is_type || 0;
    if (uriType == 0) router.push(url);
    if (uriType == 1) router.back();
  },

  /**
   * vue 路由跳转
   * @param {String} url [跳转的url地址]
   * @param {Object} type [跳转的类型]
   */
  toast: (msg, is_type) => {
    let msgType = is_type || 0;
    if (msgType == 0) return ElMessage.error(msg);
    if (msgType == 1) return ElMessage.success(msg);
    if (msgType == 2) return ElMessage.warning(msg);
    return ElMessage.info(msg);
  },
};
