import md5 from "js-md5";

const DEFAULT_PRIMARY = "#409eff"; // 默认主题颜色
export const APP_KEY = "qYyQrPNa"; // 密钥
export const DEFAULT_URI = ["Admin", "Partner", "User"]; // 默认的角色信息
export const REG_PROHIBIT = /login|register|forget_password/i;

// 加载新组件
export function _import(file) {
  return () => import(/* @vite-ignore */ "../views/" + file + ".vue");
}

// 打开一个新窗口
export function _open(url, newWin = false) {
  if (newWin) window.open(url);
  if (!newWin) window.location.href = url;
}

// 定义地址链接
export function formatUrl(value) {
  // console.log(value)
  if (value.indexOf("://") != -1) {
    // 判断是http 还是https
    const url = indow.location.href;
    let suf = "http://";
    if (url.indexOf("https://") > -1) {
      suf = "https://";
    }
    value = suf + window.location.host + value;
  }
  return value;
}

export function makeSignature(params) {
  const sortedKeys = Object.keys(params).sort();
  const queryString = sortedKeys
    .map(
      (key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
    )
    .join("&");
  return md5(queryString + APP_KEY + 1);
}

// 格式化时间戳
export function formatTime(timeVal, type = true) {
  var d = Math.floor(timeVal / (24 * 3600));
  var h = Math.floor((timeVal - 24 * 3600 * d) / 3600);
  var m = Math.floor((timeVal - 24 * 3600 * d - h * 3600) / 60);
  var s = Math.floor(timeVal - 24 * 3600 * d - h * 3600 - m * 60);
  // console.log(d + '天' + hh + '时' + mm + '分' + ss + '秒'); // 打印出转换后的时间
  //  当时分秒小于10的时候补0
  var hh = h < 10 ? "0" + h : h;
  var mm = m < 10 ? "0" + m : m;
  var ss = s < 10 ? "0" + s : s;
  // this.seckills.format_time =  d + '天' + hh + '时' + mm + '分' + ss + '秒';
  if (!type) return hh + " : " + mm + " : " + ss;
  return d + " 天 " + hh + " 时 " + mm + " 分 " + ss + " 秒";
}

// 获取本地语种
export function getLang() {
  let lang = navigator.language || navigator.userLanguage;
  if (
    localStorage.getItem("qw_language") &&
    localStorage.getItem("qw_language") != ""
  ) {
    lang = localStorage.getItem("qw_language");
  } else {
    localStorage.setItem("qw_language", lang);
  }
  return lang;
}

export function loadLangElementPlus(lang, e) {
  if (["zh-CN", "zh-SG"].includes(lang)) {
    return e.zhCn;
  }
  if (["zh-TW", "zh-HK"].includes(lang)) {
    return e.zhTw;
  }
  if (
    [
      "en-US",
      "en-UK",
      "en-EG",
      "en-AU",
      "en-CA",
      "en-NZ",
      "en-IE",
      "en-ZA",
      "en-JM",
      "en-BZ",
      "en-TT",
      "en-GB",
    ].includes(lang)
  ) {
    return e.en;
  }
  if (["vi"].includes(lang)) {
    return e.vi;
  }
  if (["ja-JP"].includes(lang)) {
    return e.ja;
  }
  if (["th"].includes(lang)) {
    return e.th;
  }
  if (["de-CH", "de-AT", "de-LU", "de-LI"].includes(lang)) {
    return e.de;
  }
  if (
    [
      "es-AR",
      "es-CR",
      "es-GT",
      "es-PA",
      "es-DO",
      "es-MX",
      "es-VE",
      "es-CO",
      "es-PA",
      "es-PE",
      "es-EC",
      "es-CL",
      "es-UY",
      "es-PY",
      "es-BO",
      "es-UY",
    ].includes(lang)
  ) {
    return e.es;
  }
  if (["ar"].includes(lang.split("-")[0])) {
    return e.pt;
  }
  if (["ru"].includes(lang.split("-")[0])) {
    return e.ru;
  }
  if (["pt"].includes(lang)) {
    return e.pt;
  }
  if (["pt-BR"].includes(lang)) {
    return e.ptBr;
  }
}
