export const H = {
  // 是否为空
  isEmpty(e) {
    if (e == undefined || e == null || e == "" || e == NaN) return true;
    return false;
  },

  /**
   * 验证手机号码
   * @param val 要验证的手机号
   */
  isPhone(val) {
    if (!/^1[23456789]\d{9}$/.test(val)) return false;
    return true;
  },

  /**
   * 去除字符串空格
   * @param str 要处理的字符串
   * @param type 1：所有空格 2：前后空格 3：前空格 4：后空格
   */
  strTrim(str, type) {
    switch (type) {
      case 1:
        return str.replace(/\s+/g, "");
      case 2:
        return str.replace(/(^\s*)|(\s*$)/g, "");
      case 3:
        return str.replace(/(^\s*)/g, "");
      case 4:
        return str.replace(/(\s*$)/g, "");
      default:
        return str;
    }
  },

  /**
   * 生成指定长度随机字符串
   * @param len 要生成字符串的长度
   */
  getRandom(len) {
    len = len || 32;
    let chars = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678";
    let maxPos = chars.length;
    let pwd = "";
    for (let i = 0; i < len; i++) {
      pwd += chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
  },

  /**
   * 检测密码强度
   * @param str 字符串
   * @returns 1：密码弱 2：密码中等 3：密码强 4：密码很强
   */
  checkPwd(str) {
    var nowLv = 0;
    if (str.length < 6) {
      return nowLv;
    }
    if (/[0-9]/.test(str)) {
      nowLv++;
    }
    if (/[a-z]/.test(str)) {
      nowLv++;
    }
    if (/[A-Z]/.test(str)) {
      nowLv++;
    }
    if (/[\.|-|_]/.test(str)) {
      nowLv++;
    }
    return nowLv;
  },

  /**
   * 判断是在手机端|平板端|PC端
   * @returns 0：手机端 1：PC端 2：平板端
   */
  os() {
    var ua = navigator.userAgent;
    (isWindowsPhone = /(?:Windows Phone)/.test(ua)),
      (isSymbian = /(?:SymbianOS)/.test(ua) || isWindowsPhone),
      (isAndroid = /(?:Android)/.test(ua)),
      (isFireFox = /(?:Firefox)/.test(ua)),
      (isChrome = /(?:Chrome|CriOS)/.test(ua)),
      (isTablet =
        /(?:iPad|PlayBook)/.test(ua) ||
        (isAndroid && !/(?:Mobile)/.test(ua)) ||
        (isFireFox && /(?:Tablet)/.test(ua))),
      (isPhone = /(?:iPhone)/.test(ua) && !isTablet),
      (isPc = !isPhone && !isAndroid && !isSymbian);
    if (isAndroid || isPhone) {
      return 0;
    } else if (isPc) {
      return 1;
    } else if (isTablet) {
      return 2;
    }
  },

  /**
   * 判断当前环境是否是微信环境
   */
  isWeixin() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
      return true;
    } else {
      return false;
    }
  },
  // 格式化金额
  formatFloat(value, length = 2) {
    let tempNum = 0;
    let s, temp;
    let s1 = value + "";
    let start = s1.indexOf(".");
    if (s1.substr(start + length + 1, 1) >= 5) {
      tempNum = 1;
    }
    temp = Math.pow(10, length);
    s = Math.floor(value * temp) + tempNum;
    return s / temp;
  },

  // 这个可用
  formatFloat2(value, length = 2) {
    let s1 = value.toString();
    let start = s1.indexOf(".");

    // 如果原始值没有小数点，则直接返回原值
    if (start === -1) {
      return s1;
    }

    // 构造一个放大10的幂次方，使得小数点后只剩下length位
    let multiplier = Math.pow(10, start + s1.length - 1 - length);

    // 放大数值并四舍五入
    let tempNum = Math.round(value * multiplier) / multiplier;

    // 使用toFixed来格式化字符串，并返回补0的结果
    return tempNum.toFixed(length);
  },

  // 获取可见页面网页大小
  getScreenWidth() {
    return window.innerWidth || document.body.clientWidth;
  },

  // 获取对应的用户信息
  getChatingInfo(id, data) {
    if (data.group_id > 0) {
      return {
        nickname: data.group_nickname || " - ",
        avatar: data.group_avatar || this.getImgUrl("group_avatar.png"),
      };
    }
    if (id == data.send_id) {
      return {
        id: data.receive_id || 0,
        nickname: data.receive_nickname || " - ",
        avatar: data.receive_avatar || undefined,
      };
    } else {
      return {
        id: data.send_id || 0,
        nickname: data.send_nickname || " - ",
        avatar: data.send_avatar || undefined,
      };
    }
  },

  // 获取图片地址
  getImgUrl(name) {
    return new URL(`../assets/${name}`, import.meta.url).href;
  },
};
