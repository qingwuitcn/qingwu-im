import { createI18n } from "vue-i18n";
import messages from "../locales/index"; // 假设你有一个 index.js 来导出所有语言信息
import { getLang } from "./config";
// 根据游览器获取语种
let lang = getLang();

const i18n = createI18n({
  legacy: false,
  locale: lang, // 设置默认语言
  fallbackLocale: "en-US", // 设置回退语言
  messages, // 加载所有语言信息
});

export default i18n;
