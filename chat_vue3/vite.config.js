import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";
import AutoImport from "unplugin-auto-import/vite";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 5173, // 端口
    proxy: {
      "/api": {
        // 请求接口中要替换的标识
        target: "http://127.0.0.1:3006", // 代理地址
        changeOrigin: true, // 是否允许跨域
        secure: true,
        rewrite: (path) => path.replace(/^\/api/, "/v1/"), // api标志替换为''
      },
      "/api/ws": {
        // 请求接口中要替换的标识
        target: "ws://127.0.0.1:3006", // 代理地址
        // changeOrigin: "both",
        ws: true, //websocket代理设置
        rewrite: (path) => path.replace(/^\/api\/ws/, "/v1/ws"), // api标志替换为''
      },
      "/uploads": {
        // 请求接口中要替换的标识
        target: "http://127.0.0.1:3006", // 代理地址
        changeOrigin: true, // 是否允许跨域
        secure: true,
        // rewrite: (path) => path.replace(/^\/api/, '/v1/') // api标志替换为''
      },
    },
  },
  plugins: [
    vue(),
    AutoImport({
      // include: [
      //     /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
      //     /\.vue$/,
      // ],
      // imports: [
      // ],
      // resolvers:{qInput},
      dirs: ["./src/plugins/h*.js"],
      vueTemplate: true,
      // injectAtEnd: false,
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});
